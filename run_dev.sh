source ~/.bashrc
nvm use

ROOT=$(dirname "$0")
ROOT=$(cd "$ROOT" && pwd)

if [ ! -d "$ROOT/node_modules" ]; then
    npm install
fi

npm start
