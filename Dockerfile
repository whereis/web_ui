FROM node:12.13.1-buster-slim as base

WORKDIR /root/app

#seperate builders because npm reports security issues and is more quick to prepare dev

FROM base as builder_dev
COPY . /root/app
RUN npm install

FROM base as dev
COPY --from=builder_dev /root/app/package.json /root/app/
COPY --from=builder_dev /root/app/public /root/app/public
COPY --from=builder_dev /root/app/src /root/app/src
COPY --from=builder_dev /root/app/node_modules /root/app/node_modules
EXPOSE 3000
CMD npm start

FROM base as builder_prod
COPY . /root/app
RUN yarn && yarn build

#FROM base as prod #TODO implement with a web server
