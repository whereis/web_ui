/**
 * This is a file with global configs and other things that might be important
 * to stand as global.
 */
// const custom_auth_base = 'http://wmservices.wavesys.pt'; // change this to set a custom remote, otherwise it will use the window location(browser url)
const custom_auth_base = 'http://10.0.0.201:5000'; // change this to set a custom remote, otherwise it will use the window location(browser url)
//const custom_auth_base = null; // change this to set a custom remote, otherwise it will use the window location(browser url)

export const auth_base = custom_auth_base || 'https://' + window.location.hostname + ':8443';
export const superagent_retry = 0;  /* quando dá 500 o superagent estava a refazer o pedido 3X */ //set number of retries to the superagent requests (ONLY for GET REQUESTS)
export const superagent_timeout_response = 60000;                    //sets maximum time to wait for the first byte to arrive from the server (miliseconds)
export const superagent_timeout_deadline = 120000;                   // but allow 1 minute for the file to finish loading. (miliseconds)

export const use_sentry = false;