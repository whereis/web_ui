import { auth_base, superagent_retry, superagent_timeout_deadline, superagent_timeout_response } from './globals';
import request from 'superagent';

export const url_from_uri = (refeer) => {
  //return auth_base + '/reporter' + refeer;
  return auth_base + '/' + refeer;
}

export const reporter_routes = {

  /**
   * Domains
   */

  //GET, PUT, DELETE
  domainById(id) {
    return 'domains/' + id;
  },

  domainID: 'domains/*',

  //GET, POST
  domains: 'domains',

  /**
   * Venues
   */

  //PUT, DELETE
  venueById(domain_id, venue_id) {
    return `${domain_id}/venues/${venue_id}`;
  },

  //GET, POST
  venuesByDomId(domain_id) {
    return `${domain_id}/venues`;
  },

  //PUT, DELETE
  venueByIdAcl: "reports/*/*",
  // GET, POST
  venuesByDomIdAcl: "venues/*",

  /**
   * Buildings
   */

  //POST
  buildingByVenueId(domain_id, venue_id) {
    return `${domain_id}/buildings/${venue_id}`;
  },

  //GET
  buildingsByDomId(domain_id) {
    return `${domain_id}/buildings`;
  },

  //GET
  buildingByVenueIdAcl: "buildings/*/*",
  //POST
  buildingsByDomIdAcl: "buildings/*",


  /**
   * Layouts
   */

  // GET, POST
  layoutsBybuildingId(domain_id, building_id) {
    return `${domain_id}/layouts/${building_id}`;
  },
  // GET
  layoutsByfloorId(domain_id, building_id, floor_id) {
    return `${domain_id}/layouts/${building_id}/${floor_id}`;
  },
  // GET, POST
  layoutsBybuildingIdAcl: "layouts/*/*",


  /**
   * Gateways
   */

  // GET, POST
  gatewaysBydomainId(domain_id) {
    return `${domain_id}/gateways`;
  },

  // GET, POST
  gatewaysBydomainIdAcl: "gateways/*",

  // GET, PUT
  gatewaysByVenueOrMac(domain_id, venue_or_mac) {
    return `gateways/${domain_id}/${venue_or_mac}`
  },
  // GET, PUT
  gatewaysByVenueOrMacAcl: "gateways/*/*",

  /**
   * Beacons
   */

  // GET, POST
  beaconsBydomainId(domain_id) {
    return `${domain_id}/beacons`;
  },

  // GET, POST
  beaconsBydomainIdAcl: "beacons/*",

  // GET, PUT
  beaconsByEquipment(domain_id, equipment_id) {
    return `${domain_id}/beacons/${equipment_id}`
  },
  // GET, PUT
  beaconsByEquipmentAcl: "beacons/*/*",

  /**
   * Beacon Status
   */

  // GET
  beaconsStatus(domain_id) {
    return `${domain_id}/beacon_status`;
  },

  // GET
  beaconsStatusAcl: "beacon_status/*",

  /**
   * Beacon History
   */

  // GET
  beaconsHistory(domain_id) {
    return `beacon_history/${domain_id}`
  },
  // GET
  beaconsHistoryAcl: "beacon_history/*",
}

/**
 * ----------------- DOMAINS ---------------
 */

export function getDomains() {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.domains))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function getDomain(domain_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.domainById(domain_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function postDomains(body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .post(url_from_uri(reporter_routes.domains))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function putDomain(domain_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .put(url_from_uri(reporter_routes.domainById(domain_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function deleteDomain(domain_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .delete(url_from_uri(reporter_routes.domainById(domain_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}


/**
 * ----------------- VENUES ---------------
 */

export function getVenues(domain_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.venuesByDomId(domain_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function postVenues(domain_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .post(url_from_uri(reporter_routes.venuesByDomId(domain_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function putVenue(domain_id, venue_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .put(url_from_uri(reporter_routes.venueById(domain_id, venue_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function deleteVenue(domain_id, venue_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .delete(url_from_uri(reporter_routes.venueById(domain_id, venue_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}


/**
 * ----------------- BUILDINGS ---------------
 */

export function getBuildings(domain_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.buildingsByDomId(domain_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function postBuilding(domain_id, venue_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .post(url_from_uri(reporter_routes.buildingByVenueId(domain_id, venue_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}


/**
 * ----------------- BUILDINGS ---------------
 */

export function getLayouts(domain_id, building_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.layoutsBybuildingId(domain_id, building_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function getLayoutFloor(domain_id, building_id, floor_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.layoutsByfloorId(domain_id, building_id, floor_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function postLayouts(domain_id, building_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .post(url_from_uri(reporter_routes.layoutsBybuildingId(domain_id, building_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}


/**
 * ----------------- GATEWAYS ---------------
 */

export function getGateways(domain_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.gatewaysBydomainId(domain_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function postGateways(domain_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .post(url_from_uri(reporter_routes.gatewaysBydomainId(domain_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function putGateway(domain_id, mac_address, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .put(url_from_uri(reporter_routes.gatewaysByVenueOrMac(domain_id, mac_address)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function getGatewayByVenue(domain_id, venue_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .delete(url_from_uri(reporter_routes.gatewaysByVenueOrMac(domain_id, venue_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

/**
 * ----------------- BEACONS ---------------
 */

export function getBeacons(domain_id,query) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.beaconsBydomainId(domain_id)))
    .query(query)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function postBeacons(domain_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .post(url_from_uri(reporter_routes.beaconsBydomainId(domain_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function putBeacon(domain_id, equipment_id, body) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .put(url_from_uri(reporter_routes.beaconsByEquipment(domain_id, equipment_id)))
    .type('json')
    .send(body)
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

export function deleteBeacon(domain_id, equipment_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .delete(url_from_uri(reporter_routes.beaconsByEquipment(domain_id, equipment_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

/**
 * ----------------- BEACON STATUS ---------------
 */

export function getBeaconStatus(domain_id, query) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.beaconsStatus(domain_id)))
    .query(query || {})
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}

/**
 * ----------------- BEACON HISTORY ---------------
 */

export function getBeaconHistory(domain_id) {
  //const idToken = localStorage.getItem('id_token');
  return request
    .get(url_from_uri(reporter_routes.beaconsHistory(domain_id)))
    .retry(superagent_retry)
    .timeout({
      response: superagent_timeout_response,
      deadline: superagent_timeout_deadline,
    });
  //.set('authorization', 'Bearer ' + idToken);
}