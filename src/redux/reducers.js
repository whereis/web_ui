import Authentication from "./where_is/authentication/reducer";
import Domains from "./where_is/domains/reducer";
import Venues from "./where_is/venues/reducer";
import Beacons from "./where_is/beacons/reducer";
import Buildings from "./where_is/buildings/reducer";
import Gateways from "./where_is/gateways/reducer";
import Layouts from "./where_is/layouts/reducer";
import App from "./App/reducer";

import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

export default history =>
  combineReducers({
    router: connectRouter(history),
    App,
    Authentication,
    Domains,
    Venues,
    Beacons,
    Buildings,
    Gateways,
    Layouts
  });
