import { all } from 'redux-saga/effects';
import wiDomSagas from './where_is/domains/saga'
import wiVenSagas from './where_is/venues/saga'
import wiBeaSagas from './where_is/beacons/saga'
import wiBuiSagas from './where_is/buildings/saga'
import wiGWSagas from './where_is/gateways/saga'
import wiLaySagas from './where_is/layouts/saga'

export default function* rootSaga(getState) {
  yield all([wiDomSagas(), wiVenSagas(), wiBeaSagas(), wiBuiSagas(), wiGWSagas(), wiLaySagas()]);
}
