import { applyMiddleware, compose, createStore } from 'redux';
import { createBrowserHistory } from 'history';
// import logger from 'redux-logger';

import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';

import createRootReducer from './reducers';
import rootSaga from './sagas';

// create browser history manager
export const history = createBrowserHistory();

// create the root reducer using the history manager
const rootReducer = createRootReducer(history);

// instantiate the saga middleware
const sagaMiddleware = createSagaMiddleware();

// create the store
export const store = createStore(
    // with a persistant reducer (saves things on local storage)
        rootReducer,
        compose(
        applyMiddleware(
            // provide any middlewares here
            thunk,
            sagaMiddleware,
            /* logger */
        )
    )
);

// run the saga middleware
sagaMiddleware.run(rootSaga);
