import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  loadingMain: false
});

export default function appReducer(state = initState, action) {
  switch (action.type) {
    case actions.DISABLE_LOADING:
      return state.set("loadingMain", false);
    case actions.ENABLE_LOADING:
      return state.set("loadingMain", true);

    default:
      return state;
  }
}
