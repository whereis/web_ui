const actions = {
  ENABLE_LOADING: "app/ENABLE_LOADING",
  enableLoading: () => ({
    type: actions.ENABLE_LOADING
  }),
  DISABLE_LOADING: "app/DISABLE_LOADING",
  disableLoading: () => ({
    type: actions.DISABLE_LOADING
  })
};
export default actions;
