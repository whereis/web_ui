import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import actions from './actions';
import {getVenues, postVenues, putVenue, deleteVenue} from '../../../config/backend/whereis'


export function* VenuesRequest() {
  yield takeEvery(actions.GET_VENUE_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getVenues, payload.domain_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_VENUE_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_VENUE_WHEREIS_SUCCESS, venue: response.body, });
    } else {
      yield put({ type: actions.GET_VENUE_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export function* AddVenue() {
  yield takeEvery(actions.POST_VENUE_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(postVenues, payload.domain_id, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.POST_VENUE_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 201) {
      yield put({ type: actions.POST_VENUE_WHEREIS_SUCCESS});
    } else {
      yield put({ type: actions.POST_VENUE_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* UpdateVenue() {
  yield takeEvery(actions.UPDATE_VENUE_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(putVenue, payload.domain_id, payload.venue_id, payload.body);
    }
    catch (err) {
      if (!err.status) {
        yield put({ type: actions.UPDATE_VENUE_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.UPDATE_VENUE_WHEREIS_SUCCESS});
    } else {
      yield put({ type: actions.UPDATE_VENUE_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export function* DeleteVenue() {
  yield takeEvery(actions.DELETE_VENUE_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(deleteVenue, payload.domain_id, payload.venue_id);
    }
    catch (err) {
      if (!err.status) {
        yield put({ type: actions.DELETE_VENUE_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.DELETE_VENUE_WHEREIS_SUCCESS});
    } else {
      yield put({ type: actions.DELETE_VENUE_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export default function* rootSaga() {
  yield all([
    fork(VenuesRequest),
    fork(AddVenue),
    fork(UpdateVenue),
    fork(DeleteVenue)
  ]);
}
