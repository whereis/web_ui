import { Map } from 'immutable';
import actions from './actions';


const initState = new Map({
  venues: null,
  network_error: false,
  unexpected_error: false,
  add_venue_error: null,
  delete_venue_error: null,
  update_venue_error: null
});

export default function venueReducer(state = initState,
  action) {
  switch (action.type) {
    case actions.GET_VENUE_WHEREIS:
      return state
        .set('venues', null)
    case actions.GET_VENUE_WHEREIS_SUCCESS:
      return state
        .set('venues', action.venue.data)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_VENUE_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);
    case actions.POST_VENUE_WHEREIS:
      return state.set('add_venue_error', null);
    case actions.POST_VENUE_WHEREIS_SUCCESS:
      return state.set('add_venue_error', false);
    case actions.POST_VENUE_WHEREIS_ERROR:
      return state.set('add_venue_error', {
        code: action.error,
        response: action.resp
      });
    case actions.DELETE_VENUE_WHEREIS:
      return state
        .set('delete_venue_error', null);
    case actions.DELETE_VENUE_WHEREIS_SUCCESS:
      return state
        .set('delete_venue_error', false);
    case actions.DELETE_VENUE_WHEREIS_ERROR:
      return state
        .set('delete_venue_error', {
          code: action.error,
          response: action.resp
        });

    case actions.UPDATE_VENUE_WHEREIS:
        return state
          .set('update_venue_error', null);
      case actions.UPDATE_VENUE_WHEREIS_SUCCESS:
        return state
          .set('update_venue_error', false);
      case actions.UPDATE_VENUE_WHEREIS_ERROR:
        return state
          .set('update_venue_error', {
            code: action.error,
            response: action.resp
          });
    default:
      return state;
  }
}
