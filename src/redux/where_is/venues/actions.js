const venuesActions = {
  POST_VENUE_WHEREIS: 'POST_VENUE_WHEREIS',
  POST_VENUE_WHEREIS_SUCCESS: 'POST_VENUE_WHEREIS_SUCCESS',
  POST_VENUE_WHEREIS_ERROR: 'POST_VENUE_WHEREIS_ERROR',

  GET_VENUE_WHEREIS: 'GET_VENUE_WHEREIS',
  GET_VENUE_WHEREIS_SUCCESS: 'GET_VENUE_WHEREIS_SUCCESS',
  GET_VENUE_WHEREIS_ERROR: 'GET_VENUE_WHEREIS_ERROR',

  UPDATE_VENUE_WHEREIS: 'UPDATE_VENUE_WHEREIS',
  UPDATE_VENUE_WHEREIS_SUCCESS: 'UPDATE_VENUE_WHEREIS_SUCCESS',
  UPDATE_VENUE_WHEREIS_ERROR: 'UPDATE_VENUE_WHEREIS_ERROR',

  DELETE_VENUE_WHEREIS: 'DELETE_VENUE_WHEREIS',
  DELETE_VENUE_WHEREIS_SUCCESS: 'DELETE_VENUE_WHEREIS_SUCCESS',
  DELETE_VENUE_WHEREIS_ERROR: 'DELETE_VENUE_WHEREIS_ERROR',


  addVenue: (domain_id, name, description='', status='active') => ({
    type: venuesActions.POST_VENUE_WHEREIS,
    domain_id,
    body:{
      name,
      description,
      status
    }
  }),

  getVenue: (domain_id, venue_id) => ({
    type: venuesActions.GET_VENUE_WHEREIS,
    domain_id,
    venue_id
  }),


  updateVenue: (domain_id, venue_id, {name, description, status}) => ({
    type: venuesActions.UPDATE_VENUE_WHEREIS,
    venue_id,
    domain_id,
    body:{
      name: name == null ? undefined : name,
      description: description == null ? undefined : description,
      status: status == null ? undefined : status
    }
  }),

  deleteVenue: (domain_id, venue_id) => ({
    type: venuesActions.DELETE_VENUE_WHEREIS,
    domain_id,
    venue_id
  }),

};
export default venuesActions;
