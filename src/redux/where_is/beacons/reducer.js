import { Map } from 'immutable';
import actions from './actions';


const initState = new Map({
  beacons: null,
  beacon_status: null,
  beacon_status_filtered: null,
  beacon_history: null,
  network_error: false,
  unexpected_error: false,
  add_beacon_error: null,
  delete_gateway_error: null,
  update_beacon_error: null
});

export default function gwReducer(state = initState,
  action) {
  switch (action.type) {
    case actions.GET_BEACONS_WHEREIS:
      return state
        .set('beacons', null)
    case actions.GET_BEACONS_WHEREIS_SUCCESS:
      return state
        .set('beacons', action.beacons["data"])
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_BEACONS_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);

    case actions.GET_BEACON_STATUS_WHEREIS_SUCCESS:
      return state
        .set('beacon_status', action.beacon_status["data"])
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_BEACON_STATUS_WHEREIS:
      return state
        .set('beacon_status', null)
    case actions.GET_BEACON_STATUS_FILTERED_SUCCESS:
      return state
        .set('beacon_status_filtered', action.beacon_status_filtered)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_BEACON_STATUS_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);

    case actions.GET_BEACON_HISTORY_WHEREIS:
      return state
        .set('beacon_history', null)
    case actions.GET_BEACON_HISTORY_WHEREIS_SUCCESS:
      return state
        .set('beacon_history', action.beacon_history)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_BEACON_HISTORY_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);

    case actions.POST_BEACON_WHEREIS:
      return state.set('add_beacon_error', null);
    case actions.POST_BEACON_WHEREIS_SUCCESS:
      return state.set('add_beacon_error', false);
    case actions.POST_BEACON_WHEREIS_ERROR:
      return state.set('add_beacon_error', {
        code: action.error,
        response: action.resp
      });
    case actions.DELETE_BEACON_WHEREIS:
      return state
        .set('delete_beacon_error', null);
    case actions.DELETE_BEACON_WHEREIS_SUCCESS:
      return state
        .set('delete_beacon_error', false);
    case actions.DELETE_BEACON_WHEREIS_ERROR:
      return state
        .set('delete_beacon_error', {
          code: action.error,
          response: action.resp
        });

    case actions.UPDATE_BEACON_WHEREIS:
      return state
        .set('update_beacon_error', null);
    case actions.UPDATE_BEACON_WHEREIS_SUCCESS:
      return state
        .set('update_beacon_error', false);
    case actions.UPDATE_BEACON_WHEREIS_ERROR:
      return state
        .set('update_beacon_error', {
          code: action.error,
          response: action.resp
        });
    default:
      return state;
  }
}
