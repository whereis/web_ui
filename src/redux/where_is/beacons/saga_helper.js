import {  getBeaconStatus } from '../../../config/backend/whereis'
import { put, call } from 'redux-saga/effects';
import actions from './actions';

export default function* get_beacons_status(domain_id) {

    try {
        return yield call(getBeaconStatus, domain_id);
    }
    catch (err) {
        if (err.timeout) {
            // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
        }
        else if (!err.status) {
            yield put({ type: actions.GET_BEACON_STATUS_WHEREIS_ERROR, error: -1, resp: err });
        }
        else if (err.status === 403 || err.status === 401) {
            yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
        } else {
            yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
        }
        return;
    }

}