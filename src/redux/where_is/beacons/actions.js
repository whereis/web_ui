const beaconsActions = {
  GET_BEACONS_WHEREIS: 'GET_BEACONS_WHEREIS',
  GET_BEACONS_WHEREIS_SUCCESS: 'GET_BEACONS_WHEREIS_SUCCESS',
  GET_BEACONS_WHEREIS_ERROR: 'GET_BEACONS_WHEREIS_ERROR',

  POST_BEACON_WHEREIS: 'POST_BEACON_WHEREIS',
  POST_BEACON_WHEREIS_SUCCESS: 'POST_BEACON_WHEREIS_SUCCESS',
  POST_BEACON_WHEREIS_ERROR: 'POST_BEACON_WHEREIS_ERROR',

  GET_BEACON_HISTORY_WHEREIS: 'GET_BEACON_HISTORY_WHEREIS',
  GET_BEACON_HISTORY_WHEREIS_SUCCESS: 'GET_BEACON_HISTORY_WHEREIS_SUCCESS',
  GET_BEACON_HISTORY_WHEREIS_ERROR: 'GET_BEACON_HISTORY_WHEREIS_ERROR',

  GET_BEACON_STATUS_WHEREIS: 'GET_BEACON_STATUS_WHEREIS',
  GET_BEACON_STATUS_WHEREIS_SUCCESS: 'GET_BEACON_STATUS_WHEREIS_SUCCESS',
  GET_BEACON_STATUS_WHEREIS_ERROR: 'GET_BEACON_STATUS_WHEREIS_ERROR',

  GET_BEACON_STATUS_FILTERED_WHEREIS: 'GET_BEACON_STATUS_FILTERED_WHEREIS',
  GET_BEACON_STATUS_FILTERED_SUCCESS: 'GET_BEACON_STATUS_FILTERED_WHEREIS_SUCCESS',
  GET_BEACON_STATUS_FILTERED_ERROR: 'GET_BEACON_STATUS_FILTERED_WHEREIS_ERROR',

  UPDATE_BEACON_WHEREIS: 'UPDATE_BEACON_WHEREIS',
  UPDATE_BEACON_WHEREIS_SUCCESS: 'UPDATE_BEACON_WHEREIS_SUCCESS',
  UPDATE_BEACON_WHEREIS_ERROR: 'UPDATE_BEACON_WHEREIS_ERROR',

  DELETE_BEACON_WHEREIS: 'DELETE_BEACON_WHEREIS',
  DELETE_BEACON_WHEREIS_SUCCESS: 'DELETE_BEACON_WHEREIS_SUCCESS',
  DELETE_BEACON_WHEREIS_ERROR: 'DELETE_BEACON_WHEREIS_ERROR',


  getBeacons: (domain_id,query) => ({
    type: beaconsActions.GET_BEACONS_WHEREIS,
    domain_id,
    query
  }),

  addBeacon: (domain_id,
    { building_id, venue_id, mac_address, manufactor, model, status, configuration, name }
  ) => ({
    type: beaconsActions.POST_BEACON_WHEREIS,
    domain_id,
    body: {
      building_id,
      venue_id,
      name,
      configuration,
      mac_address,
      manufactor,
      model,
      status
    }
  }),

  getBeaconStatus: (domain_id, query) => ({
    type: beaconsActions.GET_BEACON_STATUS_WHEREIS,
    domain_id,
    query
  }),

  getBeaconStatus_building_id: (domain_id, building_id, query) => ({
    type: beaconsActions.GET_BEACON_STATUS_FILTERED_WHEREIS,
    domain_id,
    building_id,
    query
  }),

  getBeaconHistory: (domain_id) => ({
    type: beaconsActions.GET_BEACON_HISTORY_WHEREIS,
    domain_id
  }),


  updateBeacon: (domain_id, equipment_id, { venue_id, mac_address, manufactor, model, status, configuration }) => ({
    type: beaconsActions.UPDATE_BEACON_WHEREIS,
    domain_id,
    equipment_id,
    body: {
      venue_id: venue_id == null ? undefined : venue_id,
      mac_address: mac_address == null ? undefined : mac_address,
      configuration: configuration == null ? undefined : configuration,
      manufactor: manufactor == null ? undefined : manufactor,
      model: model == null ? undefined : model,
      status: status == null ? undefined : status,
    }
  }),

  deleteBeacon: (domain_id, equipment_id) => ({
    type: beaconsActions.DELETE_BEACON_WHEREIS,
    domain_id,
    equipment_id,
  }),
};
export default beaconsActions;
