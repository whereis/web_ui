import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import actions from './actions';
import { getBeacons, postBeacons, putBeacon, getBeaconHistory, getBeaconStatus, deleteBeacon } from '../../../config/backend/whereis'
import get_beacons_status from "./saga_helper"

export function* BeaconsRequest() {
  yield takeEvery(actions.GET_BEACONS_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getBeacons, payload.domain_id,payload.query);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_BEACONS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_BEACONS_WHEREIS_SUCCESS, beacons: response.body, });
    } else {
      yield put({ type: actions.GET_BEACONS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* BeaconStatusRequest() {
  yield takeEvery(actions.GET_BEACON_STATUS_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getBeaconStatus, payload.domain_id, payload.query);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_BEACON_STATUS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_BEACON_STATUS_WHEREIS_SUCCESS, beacon_status: response.body, });
    } else {
      yield put({ type: actions.GET_BEACON_STATUS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* BeaconStatusRequestFiltered() {
  yield takeEvery(actions.GET_BEACON_STATUS_FILTERED_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getBeacons, payload.domain_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_BEACON_STATUS_FILTERED_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {

      const beacons_list = response.body["data"]

      const live_beacons_response = yield call(get_beacons_status, payload.domain_id);
      if (live_beacons_response) {
        const live_beacons_list = live_beacons_response["body"]["data"]

        /* finds building_ids to add to live beacons */
        for (let index = 0; index < live_beacons_list.length; index++) {
          const current_beacon = live_beacons_list[index];

          /* find matching beacon */
          const matching_beacon = beacons_list.find(({ equipment_id }) => equipment_id === current_beacon["equipment_id"])

          if (matching_beacon) {
            Object.assign(live_beacons_list[index], matching_beacon)
          }

        }
        
        /* filters live by building_id */
        yield put({
          type: actions.GET_BEACON_STATUS_FILTERED_SUCCESS,
          beacon_status_filtered: live_beacons_list.filter(({ building_id }) => `${building_id}` === `${payload.building_id}`)
        });
      }

    } else {
      yield put({ type: actions.GET_BEACON_STATUS_FILTERED_ERROR, error: response.statusCode });
    }
  })
}

export function* BeaconHistoryRequest() {
  yield takeEvery(actions.GET_BEACON_HISTORY_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getBeaconHistory, payload.domain_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_BEACON_HISTORY_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_BEACON_HISTORY_WHEREIS_SUCCESS, beacon_history: response.body, });
    } else {
      yield put({ type: actions.GET_BEACON_HISTORY_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* AddBeacon() {
  yield takeEvery(actions.POST_BEACON_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(postBeacons, payload.domain_id, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.POST_BEACON_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 201) {
      yield put({ type: actions.POST_BEACON_WHEREIS_SUCCESS });
    } else {
      yield put({ type: actions.POST_BEACON_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* UpdateBeacon() {
  yield takeEvery(actions.UPDATE_BEACON_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(putBeacon, payload.domain_id, payload.equipment_id, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.UPDATE_BEACON_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 201) {
      yield put({ type: actions.UPDATE_BEACON_WHEREIS_SUCCESS });
    } else {
      yield put({ type: actions.UPDATE_BEACON_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* DeleteBeacon() {
  yield takeEvery(actions.DELETE_BEACON_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(deleteBeacon, payload.domain_id, payload.equipment_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.DELETE_BEACON_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.DELETE_BEACON_WHEREIS_SUCCESS });
    } else {
      yield put({ type: actions.DELETE_BEACON_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export default function* rootSaga() {
  yield all([
    fork(BeaconsRequest),
    fork(BeaconHistoryRequest),
    fork(BeaconStatusRequestFiltered),
    fork(BeaconStatusRequest),
    fork(DeleteBeacon),
    fork(AddBeacon),
    fork(UpdateBeacon)
  ]);
}
