import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import actions from './actions';
import {getBuildings, postBuilding} from '../../../config/backend/whereis'


export function* BuildingsRequest() {
  yield takeEvery(actions.GET_BUILDINGS_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getBuildings, payload.domain_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_BUILDINGS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_BUILDINGS_WHEREIS_SUCCESS, buildings: response.body, });
    } else {
      yield put({ type: actions.GET_BUILDINGS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export function* AddBuilding() {
  yield takeEvery(actions.POST_BUILDINGS_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(postBuilding, payload.domain_id, payload.venue_id, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.POST_BUILDINGS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 201) {
      yield put({ type: actions.POST_BUILDINGS_WHEREIS_SUCCESS});
    } else {
      yield put({ type: actions.POST_BUILDINGS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export default function* rootSaga() {
  yield all([
    fork(BuildingsRequest),
    fork(AddBuilding),
  ]);
}
