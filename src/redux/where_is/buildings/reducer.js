import { Map } from 'immutable';
import actions from './actions';


const initState = new Map({
  buildings: null,
  network_error: false,
  unexpected_error: false,
  add_building_error: null,
});

export default function buildingsReducer(state = initState,
  action) {
  switch (action.type) {
    case actions.GET_BUILDINGS_WHEREIS:
      return state
        .set('buildings', null)
    case actions.GET_BUILDINGS_WHEREIS_SUCCESS:
      return state
        .set('buildings', action.buildings.data)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_BUILDINGS_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);
    case actions.POST_BUILDINGS_WHEREIS:
      return state.set('add_building_error', null);
    case actions.POST_BUILDINGS_WHEREIS_SUCCESS:
      return state.set('add_building_error', false);
    case actions.POST_BUILDINGS_WHEREIS_ERROR:
      return state.set('add_building_error', {
        code: action.error,
        response: action.resp
      });
    default:
      return state;
  }
}
