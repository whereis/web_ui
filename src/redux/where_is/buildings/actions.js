const buildingsActions = {
  POST_BUILDINGS_WHEREIS: 'POST_BUILDINGS_WHEREIS',
  POST_BUILDINGS_WHEREIS_SUCCESS: 'POST_BUILDINGS_WHEREIS_SUCCESS',
  POST_BUILDINGS_WHEREIS_ERROR: 'POST_BUILDINGS_WHEREIS_ERROR',

  GET_BUILDINGS_WHEREIS: 'GET_BUILDINGS_WHEREIS',
  GET_BUILDINGS_WHEREIS_SUCCESS: 'GET_BUILDINGS_WHEREIS_SUCCESS',
  GET_BUILDINGS_WHEREIS_ERROR: 'GET_BUILDINGS_WHEREIS_ERROR',



  addBuilding: (domain_id, venue_id, name, address, description='') => ({
    type: buildingsActions.POST_BUILDINGS_WHEREIS,
    domain_id,
    venue_id,
    body:{
      name,
      description,
      address
    }
  }),

  getBuildings: (domain_id) => ({
    type: buildingsActions.GET_BUILDINGS_WHEREIS,
    domain_id
  }),

};
export default buildingsActions;
