import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import actions from './actions';
import { getDomain, getDomains, postDomains, putDomain, deleteDomain } from '../../../config/backend/whereis'
//import actionsApp from '../app/actions';

// const colorSuccess = "#E0F8E6";
// const colorError = "#F8E0E0";

export function* DomainsRequest() {
  yield takeEvery(actions.GET_DOMAINS_WHEREIS, function* () {
    let response = null;
    try {
      response = yield call(getDomains);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_DOMAINS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_DOMAINS_WHEREIS_SUCCESS, domains: response.body, });
    } else {
      yield put({ type: actions.GET_DOMAINS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* DomainRequest() {
  yield takeEvery(actions.GET_DOMAIN_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getDomain, payload.domain_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_DOMAIN_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_DOMAIN_WHEREIS_SUCCESS, domain: response.body, });
    } else {
      yield put({ type: actions.GET_DOMAIN_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export function* AddDomain() {
  yield takeEvery(actions.POST_DOMAIN_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(postDomains, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.POST_DOMAIN_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 201) {
      yield all([
        put({ type: actions.POST_DOMAIN_WHEREIS_SUCCESS }),
        put({ type: actions.GET_DOMAINS_WHEREIS })
      ])
    } else {
      yield put({ type: actions.POST_DOMAIN_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* UpdateDomain() {
  yield takeEvery(actions.UPDATE_DOMAIN_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(putDomain, payload.domain_id, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.UPDATE_DOMAIN_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.UPDATE_DOMAIN_WHEREIS_SUCCESS });
    } else {
      yield put({ type: actions.UPDATE_DOMAIN_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export function* DeleteDomain() {
  yield takeEvery(actions.DELETE_DOMAIN_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(deleteDomain, payload.domain_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.DELETE_DOMAIN_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield all([
        put({ type: actions.DELETE_DOMAIN_WHEREIS_SUCCESS }),
        put({ type: actions.GET_DOMAINS_WHEREIS })
      ])
    } else {
      yield put({ type: actions.DELETE_DOMAIN_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export default function* rootSaga() {
  yield all([
    fork(DomainsRequest),
    fork(DomainRequest),
    fork(AddDomain),
    fork(UpdateDomain),
    fork(DeleteDomain)
  ]);
}
