const domainsActions = {
  GET_DOMAINS_WHEREIS: 'GET_DOMAINS_WHEREIS',
  GET_DOMAINS_WHEREIS_SUCCESS: 'GET_DOMAINS_WHEREIS_SUCCESS',
  GET_DOMAINS_WHEREIS_ERROR: 'GET_DOMAINS_WHEREIS_ERROR',

  POST_DOMAIN_WHEREIS: 'POST_DOMAIN_WHEREIS',
  POST_DOMAIN_WHEREIS_SUCCESS: 'POST_DOMAIN_WHEREIS_SUCCESS',
  POST_DOMAIN_WHEREIS_ERROR: 'POST_DOMAIN_WHEREIS_ERROR',

  GET_DOMAIN_WHEREIS: 'GET_DOMAIN_WHEREIS',
  GET_DOMAIN_WHEREIS_SUCCESS: 'GET_DOMAIN_WHEREIS_SUCCESS',
  GET_DOMAIN_WHEREIS_ERROR: 'GET_DOMAIN_WHEREIS_ERROR',

  UPDATE_DOMAIN_WHEREIS: 'UPDATE_DOMAIN_WHEREIS',
  UPDATE_DOMAIN_WHEREIS_SUCCESS: 'UPDATE_DOMAIN_WHEREIS_SUCCESS',
  UPDATE_DOMAIN_WHEREIS_ERROR: 'UPDATE_DOMAIN_WHEREIS_ERROR',

  DELETE_DOMAIN_WHEREIS: 'DELETE_DOMAIN_WHEREIS',
  DELETE_DOMAIN_WHEREIS_SUCCESS: 'DELETE_DOMAIN_WHEREIS_SUCCESS',
  DELETE_DOMAIN_WHEREIS_ERROR: 'DELETE_DOMAIN_WHEREIS_ERROR',

  getDomains: () => ({
    type: domainsActions.GET_DOMAINS_WHEREIS,
  }),

  addDomain: (name, description='') => ({
    type: domainsActions.POST_DOMAIN_WHEREIS,
    body:{
      name,
      description
    }
  }),

  getDomain: (domain_id) => ({
    type: domainsActions.GET_DOMAIN_WHEREIS,
    domain_id
  }),


  updateDomain: (domain_id, {name, description}) => ({
    type: domainsActions.UPDATE_DOMAIN_WHEREIS,
    domain_id,
    body:{
      name: name == null ? undefined : name,
      description: description == null ? undefined : description
    }
  }),

  deleteDomain: (domain_id) => ({
    type: domainsActions.DELETE_DOMAIN_WHEREIS,
    domain_id
  }),

};
export default domainsActions;
