import { Map } from 'immutable';
import actions from './actions';


const initState = new Map({
  domains: [],
  domain: null,
  network_error: false,
  unexpected_error: false,
  add_domain_error: null,
  delete_domain_error: null,
  update_domain_error: null
});

export default function domReducer(state = initState,
  action) {
  switch (action.type) {
   /*  case actions.GET_DOMAINS_WHEREIS:
      return state
        .set('domains', null) */
    case actions.GET_DOMAINS_WHEREIS_SUCCESS:
      return state
        .set('domains', action.domains.data)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_DOMAINS_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);
    case actions.GET_DOMAIN_WHEREIS:
      return state
        .set('domain', null)
    case actions.GET_DOMAIN_WHEREIS_SUCCESS:
      return state
        .set('domain', action.domain)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_DOMAIN_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);
    case actions.POST_DOMAIN_WHEREIS:
      return state.set('add_domain_error', null);
    case actions.POST_DOMAIN_WHEREIS_SUCCESS:
      return state.set('add_domain_error', false);
    case actions.POST_DOMAIN_WHEREIS_ERROR:
      return state.set('add_domain_error', {
        code: action.error,
        response: action.resp
      });
    case actions.DELETE_DOMAIN_WHEREIS:
      return state
        .set('delete_domain_error', null);
    case actions.DELETE_DOMAIN_WHEREIS_SUCCESS:
      return state
        .set('delete_domain_error', false);
    case actions.DELETE_DOMAIN_WHEREIS_ERROR:
      return state
        .set('delete_domain_error', {
          code: action.error,
          response: action.resp
        });

    case actions.UPDATE_DOMAIN_WHEREIS:
        return state
          .set('update_domain_error', null);
      case actions.UPDATE_DOMAIN_WHEREIS_SUCCESS:
        return state
          .set('update_domain_error', false);
      case actions.UPDATE_DOMAIN_WHEREIS_ERROR:
        return state
          .set('update_domain_error', {
            code: action.error,
            response: action.resp
          });
    default:
      return state;
  }
}
