import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import actions from './actions';
import { getLayouts, postLayouts, getLayoutFloor } from '../../../config/backend/whereis'
import sim from "./saga_helper"

export function* LayoutsRequest() {
  yield takeEvery(actions.GET_LAYOUTS_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getLayouts, payload.domain_id, payload.building_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_LAYOUTS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_LAYOUTS_WHEREIS_SUCCESS, layouts: response.body, });
    } else {
      yield put({ type: actions.GET_LAYOUTS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* LayoutFloorRequest() {
  yield takeEvery(actions.GET_LAYOUT_FLOOR_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getLayoutFloor, payload.domain_id, payload.building_id, payload.floor_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_LAYOUT_FLOOR_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_LAYOUT_FLOOR_WHEREIS_SUCCESS, layouts: response.body, });
    } else {
      yield put({ type: actions.GET_LAYOUT_FLOOR_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* AllLayoutsFloorRequest() {
  yield takeEvery(actions.GET_LAYOUT_ALL_FLOOR_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getLayouts, payload.domain_id, payload.building_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_LAYOUT_ALL_FLOOR_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {

      const layouts_list = response.body["data"]
      const all_floors_data = {

      }

      for (let index = 0; index < layouts_list.length; index++) {
        const layout = layouts_list[index];

        response = yield call(sim, payload.domain_id, payload.building_id, layout.floor);
        all_floors_data[layout.floor] = response.body["data"]
      }

      yield put({
        type: actions.GET_LAYOUT_ALL_FLOOR_WHEREIS_SUCCESS,
        all_floors_data
      });

    } else {
      yield put({ type: actions.GET_LAYOUT_ALL_FLOOR_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* AddLayout() {
  yield takeEvery(actions.POST_LAYOUTS_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(postLayouts, payload.domain_id, payload.building_id, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.POST_LAYOUTS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, layout: 'layouts', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 201) {
      yield put({ type: actions.POST_LAYOUTS_WHEREIS_SUCCESS });
    } else {
      yield put({ type: actions.POST_LAYOUTS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}


export default function* rootSaga() {
  yield all([
    fork(LayoutsRequest),
    fork(LayoutFloorRequest),
    fork(AllLayoutsFloorRequest),
    fork(AddLayout),
  ]);
}
