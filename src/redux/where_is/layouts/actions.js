const layoutsActions = {
  POST_LAYOUTS_WHEREIS: 'POST_LAYOUTS_WHEREIS',
  POST_LAYOUTS_WHEREIS_SUCCESS: 'POST_LAYOUTS_WHEREIS_SUCCESS',
  POST_LAYOUTS_WHEREIS_ERROR: 'POST_LAYOUTS_WHEREIS_ERROR',

  GET_LAYOUTS_WHEREIS: 'GET_LAYOUTS_WHEREIS',
  GET_LAYOUTS_WHEREIS_SUCCESS: 'GET_LAYOUTS_WHEREIS_SUCCESS',
  GET_LAYOUTS_WHEREIS_ERROR: 'GET_LAYOUTS_WHEREIS_ERROR',

  GET_LAYOUT_FLOOR_WHEREIS: 'GET_LAYOUT_FLOOR_WHEREIS',
  GET_LAYOUT_FLOOR_WHEREIS_SUCCESS: 'GET_LAYOUT_FLOOR_WHEREIS_SUCCESS',
  GET_LAYOUT_FLOOR_WHEREIS_ERROR: 'GET_LAYOUT_FLOOR_WHEREIS_ERROR',

  GET_LAYOUT_ALL_FLOOR_WHEREIS: 'GET_LAYOUT_ALL_FLOOR_WHEREIS',
  GET_LAYOUT_ALL_FLOOR_WHEREIS_SUCCESS: 'GET_LAYOUT_ALL_FLOOR_WHEREIS_SUCCESS',
  GET_LAYOUT_ALL_FLOOR_WHEREIS_ERROR: 'GET_LAYOUT_ALL_FLOOR_WHEREIS_ERROR',

  addLayout: (domain_id, building_id, name, floor, floorplan, geometry, description = '') => ({
    type: layoutsActions.POST_LAYOUTS_WHEREIS,
    domain_id,
    building_id,
    body: {
      name,
      description,
      floor,
      geotiff: floorplan,
      geojson: geometry
    }
  }),

  getLayouts: (domain_id, building_id, floor) => ({
    type: layoutsActions.GET_LAYOUTS_WHEREIS,
    domain_id,
    building_id,
    floor
  }),

  getLayoutFloor: (domain_id, building_id, floor_id) => ({
    type: layoutsActions.GET_LAYOUT_FLOOR_WHEREIS,
    domain_id,
    building_id,
    floor_id
  }),
  getAllLayoutsFloor: (domain_id, building_id) => ({
    type: layoutsActions.GET_LAYOUT_ALL_FLOOR_WHEREIS,
    domain_id,
    building_id,
  })

};
export default layoutsActions;
