import { Map } from 'immutable';
import actions from './actions';


const initState = new Map({
  layouts: null,
  layout_floor: {},
  network_error: false,
  unexpected_error: false,
  add_layout_error: null,
});

export default function layoutsReducer(state = initState,
  action) {
  switch (action.type) {
    case actions.GET_LAYOUTS_WHEREIS:
      return state
        .set('layouts', null)
    case actions.GET_LAYOUTS_WHEREIS_SUCCESS:
      return state
        .set('layouts', action.layouts.data)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_LAYOUTS_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);

    // layout floor
    case actions.GET_LAYOUT_FLOOR_WHEREIS:
      return state
        .set('layout_floor', {})
    case actions.GET_LAYOUT_FLOOR_WHEREIS_SUCCESS:
      return state
        .set('layout_floor', action.layouts.data)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_LAYOUT_FLOOR_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);

    // temporary all floors data
    case actions.GET_LAYOUT_ALL_FLOOR_WHEREIS_SUCCESS:
      return state
        .set('all_floors_data', action.all_floors_data)
        .set('network_error', false)
        .set('unexpected_error', false);

    case actions.POST_LAYOUTS_WHEREIS:
      return state.set('add_layout_error', null);
    case actions.POST_LAYOUTS_WHEREIS_SUCCESS:
      return state.set('add_layout_error', false);
    case actions.POST_LAYOUTS_WHEREIS_ERROR:
      return state.set('add_layout_error', {
        code: action.error,
        response: action.resp
      });


    default:
      return state;
  }
}
