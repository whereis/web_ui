const actions = {
  LOGIN: "LOGIN",
  login: (write_level,domain_name) => ({
    type: actions.LOGIN,
    domain_name,
    write_level
  }),
  LOGOUT: "LOGOUT",
  logout: () => ({
    type: actions.LOGOUT
  })
};
export default actions;
