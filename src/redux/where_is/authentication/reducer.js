import { Map } from "immutable";
import actions from "./actions";

const initState = new Map({
  domain_name: undefined,
  write_level: null // null | true | false -> null = logged out
});

export default function authReducer(state = initState, action) {
  switch (action.type) {
    case actions.LOGIN:
      return state
        .set("write_level", action.write_level)
        .set("domain_name", action.domain_name);

    case actions.LOGOUT:
      return initState;

    default:
      return state;
  }
}
