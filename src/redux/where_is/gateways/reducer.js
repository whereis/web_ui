import { Map } from 'immutable';
import actions from './actions';


const initState = new Map({
  gateways: null,
  gateway: null,
  network_error: false,
  unexpected_error: false,
  add_gateway_error: null,
  // delete_gateway_error: null,
  update_gateway_error: null
});

export default function gwReducer(state = initState,
  action) {
  switch (action.type) {
    case actions.GET_GATEWAYS_WHEREIS:
      return state
        .set('gateways', null)
    case actions.GET_GATEWAYS_WHEREIS_SUCCESS:
      return state
        .set('gateways', action.gateways["data"])
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_GATEWAYS_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);
    case actions.GET_GATEWAY_WHEREIS:
      return state
        .set('gateway', null)
    case actions.GET_GATEWAY_WHEREIS_SUCCESS:
      return state
        .set('gateway', action.gateway)
        .set('network_error', false)
        .set('unexpected_error', false);
    case actions.GET_GATEWAY_WHEREIS_ERROR:
      if (action.error === 404 || action.error === -1) {
        state = initState;
        return state
          .set('network_error', true);
      }
      return state
        .set('unexpected_error', true);
    case actions.POST_GATEWAY_WHEREIS:
      return state.set('add_gateway_error', null);
    case actions.POST_GATEWAY_WHEREIS_SUCCESS:
      return state.set('add_gateway_error', false);
    case actions.POST_GATEWAY_WHEREIS_ERROR:
      return state.set('add_gateway_error', {
        code: action.error,
        response: action.resp
      });
    // case actions.DELETE_GATEWAY_WHEREIS:
    //   return state
    //     .set('delete_gateway_error', null);
    // case actions.DELETE_GATEWAY_WHEREIS_SUCCESS:
    //   return state
    //     .set('delete_gateway_error', false);
    // case actions.DELETE_GATEWAY_WHEREIS_ERROR:
    //   return state
    //     .set('delete_gateway_error', {
    //       code: action.error,
    //       response: action.resp
    //     });

    case actions.UPDATE_GATEWAY_WHEREIS:
        return state
          .set('update_gateway_error', null);
      case actions.UPDATE_GATEWAY_WHEREIS_SUCCESS:
        return state
          .set('update_gateway_error', false);
      case actions.UPDATE_GATEWAY_WHEREIS_ERROR:
        return state
          .set('update_gateway_error', {
            code: action.error,
            response: action.resp
          });
    default:
      return state;
  }
}
