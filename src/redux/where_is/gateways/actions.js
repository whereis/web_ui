const gatewaysActions = {
  GET_GATEWAYS_WHEREIS: 'GET_GATEWAYS_WHEREIS',
  GET_GATEWAYS_WHEREIS_SUCCESS: 'GET_GATEWAYS_WHEREIS_SUCCESS',
  GET_GATEWAYS_WHEREIS_ERROR: 'GET_GATEWAYS_WHEREIS_ERROR',

  POST_GATEWAY_WHEREIS: 'POST_GATEWAY_WHEREIS',
  POST_GATEWAY_WHEREIS_SUCCESS: 'POST_GATEWAY_WHEREIS_SUCCESS',
  POST_GATEWAY_WHEREIS_ERROR: 'POST_GATEWAY_WHEREIS_ERROR',

  GET_GATEWAY_WHEREIS: 'GET_GATEWAY_WHEREIS',
  GET_GATEWAY_WHEREIS_SUCCESS: 'GET_GATEWAY_WHEREIS_SUCCESS',
  GET_GATEWAY_WHEREIS_ERROR: 'GET_GATEWAY_WHEREIS_ERROR',

  UPDATE_GATEWAY_WHEREIS: 'UPDATE_GATEWAY_WHEREIS',
  UPDATE_GATEWAY_WHEREIS_SUCCESS: 'UPDATE_GATEWAY_WHEREIS_SUCCESS',
  UPDATE_GATEWAY_WHEREIS_ERROR: 'UPDATE_GATEWAY_WHEREIS_ERROR',


  getGateways: (domain_id) => ({
    type: gatewaysActions.GET_GATEWAYS_WHEREIS,
    domain_id
  }),

  addGateway: (domain_id, body) => ({
    type: gatewaysActions.POST_GATEWAY_WHEREIS,
    domain_id,
    body
  }),

  getGateway: (domain_id, venue_id) => ({
    type: gatewaysActions.GET_GATEWAY_WHEREIS,
    domain_id,
    venue_id
  }),


  updateGateway: (domain_id, mac_address, { venue_id, floor, battery, description, latitude, longitude, manufactor, model, status }) => ({
    type: gatewaysActions.UPDATE_GATEWAY_WHEREIS,
    domain_id,
    mac_address,
    body: {
      venue_id: venue_id == null ? undefined : venue_id,
      description: description == null ? undefined : description,
      floor: floor == null ? undefined : floor,
      battery: battery == null ? undefined : battery,
      latitude: latitude == null ? undefined : latitude,
      longitude: longitude == null ? undefined : longitude,
      manufactor: manufactor == null ? undefined : manufactor,
      model: model == null ? undefined : model,
      status: status == null ? undefined : status,
    }
  }),
};
export default gatewaysActions;
