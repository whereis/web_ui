import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import actions from './actions';
import {getGatewayByVenue, getGateways, postGateways, putGateway} from '../../../config/backend/whereis'

export function* GatewaysRequest() {
  yield takeEvery(actions.GET_GATEWAYS_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getGateways, payload.domain_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_GATEWAYS_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_GATEWAYS_WHEREIS_SUCCESS, gateways: response.body, });
    } else {
      yield put({ type: actions.GET_GATEWAYS_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* GatewayRequest() {
  yield takeEvery(actions.GET_GATEWAY_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(getGatewayByVenue, payload.domain_id, payload.venue_id);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.GET_GATEWAY_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.GET_GATEWAY_WHEREIS_SUCCESS, gateway: response.body, });
    } else {
      yield put({ type: actions.GET_GATEWAY_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* AddGateway() {
  yield takeEvery(actions.POST_GATEWAY_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(postGateways, payload.domain_id, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.POST_GATEWAY_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 201) {
      yield put({ type: actions.POST_GATEWAY_WHEREIS_SUCCESS});
    } else {
      yield put({ type: actions.POST_GATEWAY_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export function* UpdateGateway() {
  yield takeEvery(actions.UPDATE_GATEWAY_WHEREIS, function* (payload) {
    let response = null;
    try {
      response = yield call(putGateway, payload.domain_id, payload.mac_address, payload.body);
    }
    catch (err) {
      if (err.timeout) {
        // yield put({ type: actionsApp.SHOW_NOTIFICATION, result: "error", operation: "timeoutError", description: "Network Timeout", style: { backgroundColor: colorError } });
      }
      else if (!err.status) {
        yield put({ type: actions.UPDATE_GATEWAY_WHEREIS_ERROR, error: -1, resp: err });
      }
      else if (err.status === 403 || err.status === 401) {
        yield put({ type: 'INVALIDTOKEN', error: err.status, resp: err.response, service: 'services', });
      } else {
        yield put({ type: 'UNAUTHORIZED', error: err.status, resp: err.response, });
      }
      return;
    }
    if (response.statusCode === 200) {
      yield put({ type: actions.UPDATE_GATEWAY_WHEREIS_SUCCESS});
    } else {
      yield put({ type: actions.UPDATE_GATEWAY_WHEREIS_ERROR, error: response.statusCode });
    }
  })
}

export default function* rootSaga() {
  yield all([
    fork(GatewaysRequest),
    fork(GatewayRequest),
    fork(AddGateway),
    fork(UpdateGateway)
  ]);
}
