import React from "react";
import MapWrapper from "../../IndoorMapTab/LeafletMap.jsx";
import { Marker, iconbeacon } from "../../../custom_leaflet/all_leaflet.js";
import { InputNumber } from "antd";

class GatewayForm extends React.PureComponent {
  state = {
    lat: undefined,
    lng: undefined
  };

  mapRef = React.createRef();

  on_map_click = e => {
    // map.setView(e.target.getLatLng(),5);
    //console.log("e.latlng", e.latlng)
    this.setState(
      { lat: e.latlng["lat"], lng: e.latlng["lng"] },
      this.onChange_Trigger
    );
  };

  save_position_input = (key, value) => {
    this.setState({ [key]: value }, this.onChange_Trigger);
  };

  onChange_Trigger = () => {
    const { lat, lng } = this.state;
    const { onChange } = this.props;

    if (
      typeof lat === "number" &&
      typeof lng === "number" &&
      typeof onChange == "function"
    ) {
      if (this.mapRef) {
        this.mapRef.setView(this.state, this.mapRef.getZoom());
      }
      onChange({
        lat,
        lng
      });
    }
  };

  get_map_ref = ref => {
    if (
      ref &&
      ref.mapRef &&
      ref.mapRef.current &&
      ref.mapRef.current.leafletElement
    ) {
      this.mapRef = ref.mapRef.current.leafletElement;
    }
  };

  render() {
    const { lat, lng } = this.state;

    const latlng =
      typeof lat === "number" && typeof lng === "number"
        ? { lat, lng }
        : undefined;

    return (
      <React.Fragment>
        Lat:
        <InputNumber
          value={lat}
          onChange={v => this.save_position_input("lat", v)}
        />
        Lng:
        <InputNumber
          value={lng}
          onChange={v => this.save_position_input("lng", v)}
        />
        OR click on map:
        <div style={{ height: 500 }}>
          <MapWrapper ref={this.get_map_ref} onClick={this.on_map_click}>
            {this.props.children}
            {latlng && <Marker position={latlng} icon={iconbeacon}></Marker>}
          </MapWrapper>
        </div>
      </React.Fragment>
    );
  }
}

export default GatewayForm;
