import React from "react";
import { connect } from "react-redux";
import BuildingsActions from "../../../../redux/where_is/buildings/actions";
import LayoutsActions from "../../../../redux/where_is/layouts/actions";
import GatewaysActions from "../../../../redux/where_is/gateways/actions";
import VenuesActions from "../../../../redux/where_is/venues/actions";
import GenericForm from "../../generic_modal.jsx";
import StatusSelect from "../../../ReusableComponents/StatusSelect.jsx";
import {
  LeafletGeoTiff,
  IndoorLayer,
  IndoorLevelLayer
} from "../../../custom_leaflet/all_leaflet.js";
import DomainsSelect from "../../../ReusableComponents/DomainsSelect";

import MarkerInput from "./MarkerInput.jsx";
import { Form, Input, Select } from "antd";

class GatewayForm extends React.PureComponent {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //console.log("Received values of form: ", values);
        const latlng = values.latlng;
        delete values.latlng;
        const domain_id = values.domain_id;
        delete values.domain_id;
        this.props.add_request(domain_id, { ...values, ...latlng });
      }
    });
  };

  get_buildings = domain_id => {
    this.props.getBuildings(domain_id);
    this.props.getVenues(domain_id);
  };

  get_layouts = building_id => {
    const { form, getLayouts } = this.props;
    getLayouts(form.getFieldValue("domain_id"), building_id);
  };

  get_layout_data = floor => {
    const { form, getLayoutFloor } = this.props;
    const domain_id = form.getFieldValue("domain_id"),
      building_id = form.getFieldValue("building_id");

    getLayoutFloor(domain_id, building_id, floor);
    //console.log("floor", floor);
    /* will be used to get geotif and geojson to show on map */
  };

  render() {
    const { form, layout_floor_data } = this.props;
    const { floor, geojson, geotiff } = layout_floor_data;

    const { getFieldDecorator } = form;

    //console.log("layout_floor_data :", layout_floor_data);

    return (
      <GenericForm onOK={this.handleSubmit}>
        <Form>
          <Form.Item>
            {getFieldDecorator("domain_id", {
              rules: [{ required: true, message: "Please select domain" }]
            })(<DomainsSelect onChange={this.get_buildings} />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("venue_id", {
              rules: [{ required: true, message: "Please select venue" }]
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select venue"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) !== -1
                }
              >
                {this.props.venues.map((item, index) => (
                  <Select.Option key={index} value={item.id}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("building_id", {
              rules: [{ required: true, message: "Please select building" }]
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select building"
                optionFilterProp="children"
                onChange={this.get_layouts}
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) !== -1
                }
              >
                {this.props.buildings.map((item, index) => (
                  <Select.Option key={index} value={item.building_id}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("floor", {
              rules: [{ required: true, message: "Please select layout" }]
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select layout"
                optionFilterProp="children"
                onChange={this.get_layout_data}
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) !== -1
                }
              >
                {this.props.layouts.map((item, index) => (
                  <Select.Option key={index} value={item.floor}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("mac_address", {
              rules: [{ required: true, message: "Please input mac_address!" }]
            })(<Input placeholder="mac_address" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("ipv4_address", {
              rules: [
                { required: true, message: "Please input a ipv4_address!" }
              ]
            })(<Input placeholder="ipv4_address" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("ipv4_netmask", {
              rules: [
                { required: true, message: "Please input a ipv4_netmask!" }
              ]
            })(<Input placeholder="ipv4_netmask" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("firmware_version", {
              rules: [
                { required: true, message: "Please input a firmware_version!" }
              ]
            })(<Input placeholder="firmware_version" />)}
          </Form.Item>
          {/* <Form.Item>
                    {getFieldDecorator('battery', {
                        rules: [{ required: true, message: 'Please input a battery!' }],
                    })(<InputNumber placeholder="battery" />,
                    )}
                </Form.Item> */}
          <Form.Item>
            {getFieldDecorator("description", {
              rules: [
                { required: true, message: "Please input a description!" }
              ]
            })(<Input placeholder="description" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("manufactor", {
              rules: [{ required: true, message: "Please input a manufactor!" }]
            })(<Input placeholder="manufactor" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("model", {
              rules: [{ required: true, message: "Please input a model!" }]
            })(<Input placeholder="model" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("status", {
              rules: [{ required: true, message: "Please input a status!" }]
            })(<StatusSelect placeholder="status" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("latlng", {
              rules: [{ required: true, message: "Please input a latlng!" }]
            })(
              <MarkerInput>
                {geotiff && <LeafletGeoTiff tiff={geotiff} />}
                {geojson && (
                  <IndoorLayer no_click intialLevel={floor} data={geojson}>
                    <IndoorLevelLayer intialLevel={"" + floor} />
                  </IndoorLayer>
                )}
              </MarkerInput>
            )}
          </Form.Item>
        </Form>
      </GenericForm>
    );
  }
}

export default connect(
  state => ({
    domains: state.Domains.get("domains") || [],
    venues: state.Venues.get("venues") || [],
    layout_floor_data: state.Layouts.get("layout_floor") || {},
    buildings: state.Buildings.get("buildings") || [],
    layouts: state.Layouts.get("layouts") || []
  }),
  {
    add_request: GatewaysActions.addGateway,
    getLayouts: LayoutsActions.getLayouts,
    getVenues: VenuesActions.getVenue,
    getBuildings: BuildingsActions.getBuildings,
    getLayoutFloor: LayoutsActions.getLayoutFloor
  }
)(Form.create()(GatewayForm));
