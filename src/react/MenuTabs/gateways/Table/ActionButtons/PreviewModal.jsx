import React from "react";
import { Modal, Button } from "antd";
import MapWrapper from "../../../IndoorMapTab/LeafletMap.jsx";
import {
  Marker,
  Popup,
  iconGateway,
  GatewaysPopUpContent
} from "../../../../custom_leaflet/all_leaflet.js";
import { LeafletGeoTiff } from "../../../../custom_leaflet/all_leaflet.js";
import { connect } from "react-redux";
import LayoutsActions from "../../../../../redux/where_is/layouts/actions";

class PreviewModal extends React.PureComponent {
  state = {
    visible: false
  };

  request_floor = () => {
    const { getLayoutFloor, domain_id, building_id, floor } = this.props;
    if (
      typeof domain_id == "string" &&
      typeof building_id == "number" &&
      typeof floor == "number"
    ) {
      getLayoutFloor(domain_id, building_id, floor);
      return true;
    }
    return false;
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.visible !== this.state.visible) {
      this.request_floor();
    }
  }

  onPreviewClick = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  render() {
    const { coordinates, data } = this.props;
    const { geotiff } = data;

    return (
      <React.Fragment>
        <Button onClick={this.onPreviewClick}>Preview</Button>
        <Modal
          visible={this.state.visible}
          title="Preview Gateway"
          onCancel={this.handleCancel}
          footer={[]}
        >
          <div style={{ height: 500 }}>
            <MapWrapper position={coordinates}>
              {geotiff && <LeafletGeoTiff tiff={geotiff} />}
              <Marker position={coordinates} icon={iconGateway}>
                <Popup>
                  <GatewaysPopUpContent data={this.props} />
                </Popup>
              </Marker>
            </MapWrapper>
          </div>
        </Modal>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    data: state.Layouts.get("layout_floor") || {}
  }),
  {
    getLayoutFloor: LayoutsActions.getLayoutFloor
  }
)(PreviewModal);
