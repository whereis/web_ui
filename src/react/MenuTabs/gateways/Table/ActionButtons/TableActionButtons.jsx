import React from "react";
import PreviewModal from "./PreviewModal.jsx";

class TableActionButtons extends React.PureComponent {
  render() {
    const { data, domain_id } = this.props;
    const { coordinates, floor, building_id, description } = data;

    return (
      <React.Fragment>
        <PreviewModal
          domain_id={domain_id}
          building_id={building_id}
          floor={floor}
          coordinates={coordinates}
          description={description}
        />
      </React.Fragment>
    );
  }
}

export default TableActionButtons;
