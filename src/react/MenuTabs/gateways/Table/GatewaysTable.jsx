import React from "react";
import { connect } from "react-redux";
import GatewaysActions from "../../../../redux/where_is/gateways/actions";
import ActionButtons from "./ActionButtons/TableActionButtons.jsx";
import { Table } from "antd";

class GatewaysTable extends React.PureComponent {
  componentDidMount() {
    if (this.props.domain_id) {
      this.props.get_request(this.props.domain_id);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.domain_id && this.props.domain_id !== prevProps.domain_id) {
      this.props.get_request(this.props.domain_id);
    }
  }

  generate_columns = () => [
    {
      title: "battery",
      dataIndex: "battery",
      key: "battery"
    },
    {
      title: "description",
      dataIndex: "description",
      key: "description"
    },
    {
      title: "firmware_version",
      dataIndex: "firmware_version",
      key: "firmware_version"
    },
    {
      title: "ipv4_address",
      dataIndex: "ipv4_address",
      key: "ipv4_address"
    },
    {
      title: "ipv4_netmask",
      dataIndex: "ipv4_netmask",
      key: "ipv4_netmask"
    },
    {
      title: "mac_address",
      key: "mac_address",
      dataIndex: "mac_address"
    },
    {
      title: "manufactor",
      key: "manufactor",
      dataIndex: "manufactor"
    },
    {
      title: "model",
      key: "model",
      dataIndex: "model"
    },
    {
      title: "provisioned_on",
      key: "provisioned_on",
      dataIndex: "provisioned_on"
    },
    {
      title: "status",
      key: "status",
      dataIndex: "status"
    },
    {
      title: "updated_on",
      key: "updated_on",
      dataIndex: "updated_on"
    },
    {
      title: "venue_id",
      key: "venue_id",
      dataIndex: "venue_id"
    },
    {
      title: "Action",
      key: "action",
      render: record => (
        <ActionButtons domain_id={this.props.domain_id} data={record} />
      )
    }
  ];

  render() {
    return (
      <Table
        columns={this.generate_columns()}
        rowKey={(r, i) => i}
        dataSource={this.props.data}
      />
    );
  }
}

export default connect(
  state => ({
    data: state.Gateways.get("gateways") || []
  }),
  {
    get_request: GatewaysActions.getGateways
  }
)(GatewaysTable);
