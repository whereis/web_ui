import React from "react";
import { Button } from "antd";
import AddGatewayForm from "./Add/AddGateway.jsx";
import GatewaysTable from "./Table/GatewaysTable.jsx";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class GatewaysPage extends React.PureComponent {
  state = {
    selected_domain: undefined
  };

  onSelectDomain = selected_domain => {
    this.setState({ selected_domain });
  };

  onRefresh = () => {
    const current_domain = this.state.selected_domain;
    this.setState({ selected_domain: undefined }, () =>
      this.setState({ selected_domain: current_domain })
    );
  };

  render() {
    return (
      <React.Fragment>
        <AddGatewayForm />
        <DomainsSelect onChange={this.onSelectDomain} />
        <Button
          type="primary"
          shape="circle"
          icon="redo"
          size="large"
          onClick={this.onRefresh}
        />
        <GatewaysTable domain_id={this.state.selected_domain} />
      </React.Fragment>
    );
  }
}

export default GatewaysPage;
