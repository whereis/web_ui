import React from "react";
import { Button } from "antd";
import AddBuildingForm from "./AddBuilding.jsx";
import BuildingsTable from "./BuildingsTable.jsx";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class BuildingPage extends React.PureComponent {
  state = {
    selected_domain: undefined
  };

  onSelectDomain = selected_domain => {
    this.setState({ selected_domain });
  };

  onRefresh = () => {
    const current_domain = this.state.selected_domain;
    this.setState({ selected_domain: undefined }, () =>
      this.setState({ selected_domain: current_domain })
    );
  };

  render() {
    return (
      <React.Fragment>
        <AddBuildingForm />
        Dominio:
        <DomainsSelect onChange={this.onSelectDomain} />
        <Button
          type="primary"
          shape="circle"
          icon="redo"
          size="large"
          onClick={this.onRefresh}
        />
        <BuildingsTable domain_id={this.state.selected_domain} />
      </React.Fragment>
    );
  }
}

export default BuildingPage;
