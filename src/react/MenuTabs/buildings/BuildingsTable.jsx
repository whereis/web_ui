import React from "react"
import { connect } from "react-redux";
import BuildingsActions from "../../../redux/where_is/buildings/actions"
import { Table, /* Button */ } from 'antd';

class BuildingsTable extends React.PureComponent {

    componentDidMount() {
        if (this.props.domain_id) {
            this.props.get_request(this.props.domain_id)
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.domain_id && this.props.domain_id !== prevProps.domain_id) {
            this.props.get_request(this.props.domain_id)
        }
    }

    generate_columns = () => [
        {
            title: 'venue_id',
            dataIndex: 'venue_id',
            key: 'venue_id',
        },
        {
            title: 'building_id',
            dataIndex: 'building_id',
            key: 'building_id',
        },
        {
            title: 'name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'address',
            dataIndex: 'address',
            key: 'address',
        },
        {
            title: 'description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'provisioned_on',
            key: 'provisioned_on',
            dataIndex: 'provisioned_on',
        },
        {
            title: 'updated_on',
            key: 'updated_on',
            dataIndex: 'updated_on',
        },
        /* { no delete support
            title: 'Action',
            key: 'action',
            render: (record) => <Button
                onClick={() => this.props.delete_by_id_request(this.props.domain_id, record["id"])}  >
                Delete
                </Button>,
        } */
    ]


    render() {

        return <Table
            columns={this.generate_columns()}
            rowKey={(r, i) => i}
            dataSource={this.props.data} />
    }
}

export default connect(state => ({
    data: state.Buildings.get('buildings') || [],
}),
    {
        get_request: BuildingsActions.getBuildings,
        //delete_by_id_request: BuildingsActions,
    },
)(BuildingsTable);