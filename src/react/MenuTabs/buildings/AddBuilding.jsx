import React from "react";
import { connect } from "react-redux";
import BuildingsActions from "../../../redux/where_is/buildings/actions";
import DomainsActions from "../../../redux/where_is/domains/actions";
import VenuesActions from "../../../redux/where_is/venues/actions";
import { Form, Input, Select } from "antd";
import GenericForm from "../generic_modal.jsx";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class BuildingForm extends React.PureComponent {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(
      (err, { domain_id, venue_id, name, address, description }) => {
        if (!err) {
          //console.log("Received values of form: ", name, description);
          this.props.add_request(
            domain_id,
            venue_id,
            name,
            address,
            description
          );
        }
      }
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <GenericForm onOK={this.handleSubmit}>
        <Form>
          <Form.Item label="domain">
            {getFieldDecorator("domain_id", {
              rules: [{ required: true, message: "Please select domain" }]
            })(<DomainsSelect onChange={this.props.get_venues_by_domain} />)}
          </Form.Item>
          <Form.Item label="venue">
            {getFieldDecorator("venue_id", {
              rules: [{ required: true, message: "Please select venue" }]
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select venue"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) !== -1
                }
              >
                {this.props.venues_list.map((item, index) => (
                  <Select.Option key={index} value={item.id}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("name", {
              rules: [{ required: true, message: "Please input domain name!" }]
            })(<Input placeholder="Name" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("address", {
              rules: [{ required: true, message: "Please input a address!" }]
            })(<Input placeholder="Address" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("description")(
              <Input placeholder="Description" />
            )}
          </Form.Item>
        </Form>
      </GenericForm>
    );
  }
}

export default connect(
  state => ({
    domains_list: state.Domains.get("domains") || [],
    venues_list: state.Venues.get("venues") || []
  }),
  {
    add_request: BuildingsActions.addBuilding,
    get_domains:
      DomainsActions.getDomains /* not used sience parent already requests domains */,
    get_venues_by_domain: VenuesActions.getVenue
  }
)(Form.create()(BuildingForm));
