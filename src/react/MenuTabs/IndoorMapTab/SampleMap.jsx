import React, { Component } from 'react'
import { Button } from 'antd';
import {
  L,
  Map,
  TileLayer,
  Marker,
  Popup,
  FeatureGroup,
  Circle,
  IndoorLayer,
  IndoorLevelLayer,
  LeafletGeoTiff,
  EditControl,
  simplestyle,
  bindPopup } from "../../custom_leaflet/all_leaflet.js"

export default class App extends Component {
  state = {
    lat: 49.418544,
    lng: 8.676812,
    zoom: 18,
    geo: undefined
  }

  mapRef = React.createRef();
  FeatureGroupRef = React.createRef();

  componentDidUpdate() {
    this.mapRef.current.leafletElement.invalidateSize();
  }

  saveRef = (ref) => {
    if (ref) {
      this.FeatureGroupRef = ref
      //console.log('this.FeatureGroupRef :', this.FeatureGroupRef);

    }
  }

  onDrawStop = (event) => {
    const { layer } = event
    //console.log('event :', event);
    //console.log('event draw :', event);
    //event.layer.bindPopup('Hello');
    const geojson = layer.toGeoJSON()
    L.geoJson(geojson, {
      style: simplestyle.style,
      pointToLayer: function (feature, latlon) {
        if (!feature.properties) feature.properties = {};
        return simplestyle.marker_style(feature, latlon);
      }
    }).eachLayer(
      (l) => {
        bindPopup(l, true)
        l.addTo(this.FeatureGroupRef.leafletElement);
      }
    );
  }

  render() {
    const position = [this.state.lat, this.state.lng]

    //console.log('position :', position);
    //console.log('geo,json :', this.state.geo);

    return <React.Fragment>
      <Map ref={this.mapRef} style={{ width: "100%", height: "100%" }} center={position} zoom={this.state.zoom}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <LeafletGeoTiff tiff={this.props.uploaded_tiff} />
        <IndoorLayer >
          <IndoorLevelLayer />
        </IndoorLayer >
        <FeatureGroup ref={this.saveRef} >
          <EditControl
            position='topright'
            onCreated={this.onDrawStop}
            draw={{
              rectangle: false,
              polyline: false,
            }}
          />
          <Circle center={position} radius={200} />
        </FeatureGroup>
        <Marker position={position}>
          <Popup>
            A pretty CSS3 popup. <br /> Easily customizable.
          </Popup>
        </Marker>
      </Map>
      <Button onClick={() => this.FeatureGroupRef && this.setState({ geo: this.FeatureGroupRef.leafletElement.toGeoJSON() })}>
        export geoJson
      </Button>
    </React.Fragment>

  }
}
