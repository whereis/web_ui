import React, { Component } from 'react'
import { Map, TileLayer } from '../../custom_leaflet/all_leaflet.js'

export default class App extends Component {

  state = {
    lat: 40.663777,
    lng: -8.609107,
    zoom: 18,
    geo: undefined
  }

  mapRef = React.createRef();
  FeatureGroupRef = React.createRef();

  componentDidUpdate() {
    this.mapRef.current.leafletElement.invalidateSize();
  }

  render() {
    const position = this.props.position || [this.state.lat, this.state.lng]

    return <Map
      ref={this.mapRef}
      style={{ width: "100%", height: "100%" }}
      center={position}
      zoom={this.state.zoom}
      maxZoom={19}
      {...this.props}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
        url="https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png"
      />
      {this.props.children}
    </Map>

  }
}
