import React, { Component } from "react";
import {
  CanvasMarker,
  CanvasLayer,
  iconbeacon
} from "../../custom_leaflet/all_leaflet.js";
import gen_latlng from "./generate_position.js";

export default class MarkerManager extends Component {
  constructor(props) {
    super(props);

    let new_markers = [];
    if (props.markers_amount) {
      new_markers = this.gen_markers(props.markers_amount);
    }

    this.state = {
      markers: new_markers
    };
    this.go.bind(this);
  }

  gen_markers(markers_amount) {
    const new_markers = [];
    for (let index = 0; index < markers_amount; index++) {
      new_markers.push({
        name: "marker" + index,
        latlng: gen_latlng()
      });
    }
    return new_markers;
  }

  componentDidMount() {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.go();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.refresh_sec !== this.props.refresh_sec &&
      this.props.refresh_sec
    ) {
      // updates refesh rate on timer
      clearTimeout(this.timer);
      this.go();
    }
    if (
      prevProps.markers_amount !== this.props.markers_amount &&
      this.props.markers_amount
    ) {
      // updates number of markers
      clearTimeout(this.timer);
      this.setState(
        {
          markers: this.gen_markers(this.props.markers_amount)
        },
        this.go
      );
    }
  }

  go = async () => {
    this.timer = setTimeout(() => {
      this.setState(
        state => ({
          markers: state.markers.map(marker => ({
            name: marker["name"],
            latlng: gen_latlng()
          }))
        }),
        this.go
      );
    }, this.props.refresh_sec || 5000);
  };

  render() {
    //console.log("slining markers=", this.state.markers.length);

    return (
      <CanvasLayer>
        {this.state.markers.map(marker => (
          <CanvasMarker
            key={marker["name"]}
            position={marker["latlng"]}
            popupContent={`s: ${marker["name"]}`}
            icon={iconbeacon}
            onMarkerClick={id => {
              //console.log("id click :", id);
            }}
          />
        ))}
      </CanvasLayer>
    );
  }
}
