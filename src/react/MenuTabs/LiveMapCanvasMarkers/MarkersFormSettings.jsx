import React from "react"
import { connect } from "react-redux";
import DomainsActions from "../../../redux/where_is/domains/actions"
import { Form, InputNumber, Button } from 'antd';

class DomainForm extends React.PureComponent {
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                //console.log('Received values of form: ', values);
                this.props.onSubmit(values)
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Item>
                    {getFieldDecorator('markers_amount', { initialValue: 1 })(<InputNumber placeholder="Amount of markers" />)}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('refresh_sec', { initialValue: 1000 })(<InputNumber placeholder="refresh time" />,
                    )}
                </Form.Item>
                <Form.Item >
                    <Button type="primary" htmlType="submit">
                        Apply
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}


export default connect(null,
    { add_request: DomainsActions.addDomain, },
)(Form.create()(DomainForm));
