export default function gen_latlng(){

    const lat_max=49.41893903707716
    const lat_min=49.418496718344954

    const lng_max=8.676613569259645
    const lng_min=8.677187561988832

    const lat=(Math.random() * (lat_max - lat_min) + lat_min)//.toFixed(4)
    const lng=(Math.random() * (lng_max - lng_min) + lng_min)//.toFixed(4)

    return {
        lat,
        lng
    }

}