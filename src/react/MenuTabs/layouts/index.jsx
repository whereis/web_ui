import React from "react";
import { Select, Button } from "antd";
import LayoutsTable from "./Table/LayoutsTable.jsx";
import { connect } from "react-redux";
import BuildingsActions from "../../../redux/where_is/buildings/actions";
import AddLayout from "./AddLayout.jsx";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class LayoutsPage extends React.PureComponent {
  state = {
    selected_domain: undefined,
    selected_building: undefined
  };

  onSelectDomain = selected_domain => {
    this.props.get_buildings(selected_domain);
    this.setState({ selected_domain, selected_building: undefined });
  };

  onSelectBuilding = selected_building => {
    this.setState({ selected_building });
  };

  onRefresh = () => {
    const {
      selected_domain: current_domain,
      selected_building: current_building
    } = this.state;
    this.setState(
      { selected_domain: undefined, selected_building: undefined },
      () =>
        this.setState({
          selected_domain: current_domain,
          selected_building: current_building
        })
    );
  };

  render() {
    return (
      <React.Fragment>
        <AddLayout />
        <DomainsSelect onChange={this.onSelectDomain} />
        {this.state.selected_domain && (
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="select layout"
            optionFilterProp="children"
            defaultValue={this.state.selected_building}
            onChange={this.onSelectBuilding}
            filterOption={(input, option) =>
              option.props.children
                .toLowerCase()
                .indexOf(input.toLowerCase()) !== -1
            }
          >
            {this.props.buildings_list.map((item, index) => (
              <Select.Option key={index} value={item.building_id}>
                {item.name}
              </Select.Option>
            ))}
          </Select>
        )}
        {this.state.selected_building && (
          <Button
            type="primary"
            shape="circle"
            icon="redo"
            size="large"
            onClick={this.onRefresh}
          />
        )}
        <LayoutsTable
          domain_id={this.state.selected_domain}
          building_id={this.state.selected_building}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    buildings_list: state.Buildings.get("buildings") || []
  }),
  {
    get_buildings: BuildingsActions.getBuildings
  }
)(LayoutsPage);
