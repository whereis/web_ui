import React from "react"
import { connect } from "react-redux";
import { Table } from 'antd';
import ActionButtons from "./ActionButtons/TableActionButtons.jsx"
import LayoutsActions from "../../../../redux/where_is/layouts/actions"

class LayoutsTable extends React.PureComponent {

    componentDidMount() {
        if (this.props.domain_id && this.props.building_id) {
            this.props.get_request(this.props.domain_id, this.props.building_id)
        }
    }

    componentDidUpdate(prevProps) {
        // domain id exists
        if (this.props.domain_id) {
            // building id exists
            if (this.props.building_id && this.props.building_id !== prevProps.building_id) {
                this.props.get_request(this.props.domain_id, this.props.building_id)
            }
        }
    }

    generate_columns = () => [
        {
            title: 'building_id',
            dataIndex: 'building_id',
            key: 'building_id',
        },
        {
            title: 'floor',
            dataIndex: 'floor',
            key: 'floor',
        },
        {
            title: 'name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'provisioned_on',
            dataIndex: 'provisioned_on',
            key: 'provisioned_on',
        },
        {
            title: 'updated_on',
            dataIndex: 'updated_on',
            key: 'updated_on',
        },
        {
            title: 'Action',
            key: 'action',
            render: (record) => <ActionButtons
                data={record}
                domain_id={this.props.domain_id}
            />,
        }
    ]


    render() {

        return <Table
            columns={this.generate_columns()}
            rowKey={(r, i) => i}
            dataSource={this.props.data} />
    }
}

export default connect(state => ({
    data: state.Layouts.get('layouts') || [],
}),
    {
        get_request: LayoutsActions.getLayouts
    },
)(LayoutsTable);