import React from "react"
import { Modal, Button } from "antd"
import MapWrapper from "../../../IndoorMapTab/LeafletMap.jsx"
import { LeafletGeoTiff, IndoorLayer, IndoorLevelLayer } from "../../../../custom_leaflet/all_leaflet.js"
import { connect } from "react-redux";
import LayoutsActions from "../../../../../redux/where_is/layouts/actions"

class PreviewModal extends React.PureComponent {

    state = {
        visible: false
    }

    request_floor = () => {
        const { getLayoutFloor, domain_id, building_id, floor } = this.props
        if (typeof domain_id == "string" && typeof building_id == "number" && typeof floor == "number") {
            getLayoutFloor(domain_id, building_id, floor)
            return true
        }
        return false
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.visible !== this.state.visible) {
            this.request_floor()
        }
    }

    onPreviewClick = () => {
        this.setState({ visible: true })
    }

    handleCancel = () => {
        this.setState({ visible: false })
    }

    render() {

        const { geojson, geotiff, floor } = this.props.data
        //console.log("sim", this.props.data)
        return <React.Fragment>
            <Button
                onClick={this.onPreviewClick}
            >
                Preview
            </Button>
            <Modal
                destroyOnClose
                visible={this.state.visible}
                title="Preview layout"
                onCancel={this.handleCancel}
                footer={[]}
            >
                <div style={{ height: 500 }}>
                    <MapWrapper >
                        {geotiff && <LeafletGeoTiff tiff={geotiff} />}
                        {geojson && <IndoorLayer data={geojson}>
                            <IndoorLevelLayer intialLevel={"" + floor} />
                        </IndoorLayer >}
                    </MapWrapper>
                </div>

            </Modal>
        </React.Fragment>
    }
}


export default connect(state => ({
    data: state.Layouts.get('layout_floor') || {},
}),
    {
        getLayoutFloor: LayoutsActions.getLayoutFloor
    },
)(PreviewModal);
