import React from "react"
import PreviewModal from "./PreviewModal.jsx"

class TableActionButtons extends React.PureComponent {

    render() {
        const { data,domain_id } = this.props

        return <React.Fragment>
            <PreviewModal domain_id={domain_id}
                building_id={data.building_id}
                floor={data.floor} />
        </React.Fragment>
    }
}


export default TableActionButtons;