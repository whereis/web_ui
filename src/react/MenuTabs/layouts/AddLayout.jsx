import React from "react";
import { connect } from "react-redux";
import BuildingsActions from "../../../redux/where_is/buildings/actions";
import LayoutsActions from "../../../redux/where_is/layouts/actions";
import DrawGeoJsonInput from "./DrawGeoJsonInput.jsx";
import GenericForm from "../generic_modal.jsx";
import {
  Form,
  Input,
  InputNumber,
  Button,
  Select,
  Upload,
  Icon,
  message
} from "antd";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class LayoutForm extends React.PureComponent {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err && values.floorplan) {
        //console.log("Received values of form: ", values);

        const tif_file = values.floorplan[0]["originFileObj"];

        const base64_tif = await this.toBase64(tif_file);

        this.props.addLayout(
          values.domain_id,
          values.building_id,
          values.name,
          Number(values.floor),
          base64_tif,
          values.geojson,
          values.description
        );
        message.success("Layout Added....probably");
      }
    });
  };

  toBase64 = file =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });

  normFile = e => {
    if (e && e.file && !e.file.type.includes("tif")) {
      message.error("Only tif images supported");
    } else {
      if (Array.isArray(e)) {
        return e;
      }
      return e && e.fileList;
    }
  };

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;

    const current_upload = getFieldValue("floorplan");
    const current_floor = getFieldValue("floor");

    const show_upload =
      Array.isArray(current_upload) && current_upload.length ? false : true;

    return (
      <GenericForm onOK={this.handleSubmit}>
        <Form onSubmit={this.handleSubmit}>
          <Form.Item>
            {getFieldDecorator("domain_id", {
              rules: [{ required: true, message: "Please select domain" }]
            })(<DomainsSelect onChange={this.props.get_buildings} />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("building_id", {
              rules: [{ required: true, message: "Please select building" }]
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select building"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) !== -1
                }
              >
                {this.props.buildings_list.map((item, index) => (
                  <Select.Option key={index} value={item.building_id}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("floor", {
              rules: [{ required: true, message: "Please input floor!" }]
            })(<InputNumber placeholder="floor" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("name", {
              rules: [{ required: true, message: "Please input a name!" }]
            })(<Input placeholder="name" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("description", {
              rules: [
                { required: true, message: "Please input a description!" }
              ]
            })(<Input placeholder="Description" />)}
          </Form.Item>
          <Form.Item label="Upload">
            {getFieldDecorator("floorplan", {
              rules: [{ required: true, message: "Please upload tif!" }],
              valuePropName: "fileList",
              getValueFromEvent: this.normFile
            })(
              <Upload
                listType="picture"
                accept=".tif"
                beforeUpload={() => false}
              >
                {show_upload && (
                  <Button>
                    <Icon type="upload" />
                    Click to upload
                  </Button>
                )}
              </Upload>
            )}
          </Form.Item>
          <Form.Item label="Draw">
            {getFieldDecorator("geojson", {
              rules: [{ required: true, message: "Please draw" }]
            })(
              <DrawGeoJsonInput
                current_floor={current_floor}
                uploaded_tiff={
                  !show_upload && current_upload[0]["originFileObj"]
                }
              />
            )}
          </Form.Item>
        </Form>
      </GenericForm>
    );
  }
}

export default connect(
  state => ({
    domains: state.Domains.get("domains") || [],
    buildings_list: state.Buildings.get("buildings") || []
  }),
  {
    get_buildings: BuildingsActions.getBuildings,
    addLayout: LayoutsActions.addLayout
  }
)(Form.create()(LayoutForm));
