import React from "react";
import MapWrapper from "../IndoorMapTab/LeafletMap.jsx";
import {
  LeafletGeoTiff,
  FeatureGroup,
  EditControl,
  /* simplestyle, */
  L
  // bindPopup
} from "../../custom_leaflet/all_leaflet.js";
import uuidV4 from "uuid/v4";
import LayerStyleUI from "../../custom_leaflet/ReactStyleLayer/LayerStyleUI";

class DrawGeoJsonInput extends React.PureComponent {
  state = {
    image_url: undefined,
    geo: undefined,
    selectedLayer: undefined
  };

  saveRef = ref => {
    this.FeatureGroupRef = ref;
  };

  OnLayerClick = l => {
    this.setState({ selectedLayer: l });
  };
  OnCloseDrawer = l => {
    this.setState({ selectedLayer: undefined });
  };

  OnSubmitDrawer = (values, l) => {
    // tem aqui um bug ao ir buscar o id, nem todas a propriedades estão atualizadas com o novo id
    this.onDrawStop({ layer: l, previousID: l.feature.id, newStyle: values });
    this.setState({ selectedLayer: undefined });
  };

  onDrawStop = event => {
    const { layer, previousID, newStyle } = event,
      { current_floor } = this.props;

    /* mutate to indoor */
    // tem aqui um bug ao ir buscar o id, nem todas a propriedades estão atualizadas com o novo id
    const layer_geojson = layer.toGeoJSON();

    // saves default style
    // fillcolor comes undefined from draw
    layer_geojson["properties"]["style"] = {
      ...layer.options,
      fillColor: layer.options.color
    };
    // removes after coping everything
    layer.remove();
    // creates a unique identifier for layer
    const gen_uuid = uuidV4();

    layer_geojson["id"] = gen_uuid;

    layer_geojson["properties"] = {
      ...layer_geojson["properties"],
      type: "way",
      id: gen_uuid,
      tags: {
        buildingpart: "hall",
        height: "3.9",
        name: "012"
      },
      relations: [
        {
          role: "buildingpart",
          rel: "1370728",
          reltags: {
            height: "4",
            level: `${current_floor}`,
            name: "Erdgeschoss",
            type: "level"
          }
        }
      ],
      meta: {}
    };

    L.geoJSON(layer_geojson, layer_geojson["properties"]).eachLayer(l => {
      if (typeof l.setStyle === "function") {
        // sets default style
        l.setStyle(newStyle ? newStyle : l.defaultOptions.style);
        // binds style popup
        // OLD bindPopup(l);
        // memory leak here needs and off event
        l.on("click", () => this.OnLayerClick(l));
      }
      this.FeatureGroupRef["leafletElement"].addLayer(l);
    });

    let featuregr_with_styles = {
      type: "FeatureCollection",
      features: []
    };

    if (this.props.value) {
      featuregr_with_styles = this.props.value;
      if (previousID) {
        const existingLayerIndex = featuregr_with_styles["features"].findIndex(
          tempLayer =>
            //console.log("comparison", tempLayer, tempLayer.id, previousID)
            tempLayer.id === previousID
        );
        // removes previous saved layer (outdated new style)
        featuregr_with_styles["features"].splice(existingLayerIndex, 1);
      }
      featuregr_with_styles["features"].push(layer_geojson);
    } else {
      featuregr_with_styles["features"].push(layer_geojson);
    }

    this.props.onChange(featuregr_with_styles);
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.uploaded_tiff &&
      this.props.uploaded_tiff !== prevProps.uploaded_tiff
    ) {
      const image_url = URL.createObjectURL(this.props.uploaded_tiff);
      this.setState({ image_url });
    }
  }

  render() {
    const { image_url, selectedLayer } = this.state;

    return (
      <div style={{ height: 500 }}>
        <MapWrapper>
          <LayerStyleUI
            layer={selectedLayer}
            visible={selectedLayer !== undefined}
            onSubmit={this.OnSubmitDrawer}
            onClose={this.OnCloseDrawer}
          />
          {image_url && <LeafletGeoTiff tiff={image_url} />}
          <FeatureGroup ref={this.saveRef}>
            <EditControl
              position="topright"
              onCreated={this.onDrawStop}
              draw={{
                // rectangle: false,
                polyline: false
              }}
            />
          </FeatureGroup>
        </MapWrapper>
      </div>
    );
  }
}

export default DrawGeoJsonInput;
