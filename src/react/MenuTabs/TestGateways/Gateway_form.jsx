import React from "react"
import { Form, Input, Modal } from 'antd';

class GatewayForm extends React.PureComponent {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onSubmit(values)
            }
        });
    };

    render() {
        const { initialValue, form, visible, onSubmit } = this.props
        const { getFieldDecorator } = form;

        const default_lat = typeof initialValue.latlng === 'object' ? initialValue.latlng["lat"] : undefined
        const default_lng = typeof initialValue.latlng === 'object' ? initialValue.latlng["lng"] : undefined

        return <Modal
            visible={visible}
            onCancel={() => onSubmit(undefined)}
            onOk={this.handleSubmit}
            destroyOnClose={true}
        >
            <Form >
                <Form.Item>
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Please input domain name!' }],
                    })(<Input placeholder="Name" />)}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('lat', {
                        initialValue: default_lat,
                        rules: [{ required: true, message: 'Please input a latitude!' }],
                    })(<Input placeholder="Latitude" />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('lng', {
                        initialValue: default_lng,
                        rules: [{ required: true, message: 'Please input a longitude!' }],
                    })(<Input placeholder="Longitude" />,
                    )}
                </Form.Item>
            </Form>
        </Modal>
    }
}

export default Form.create()(GatewayForm)
