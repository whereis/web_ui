import React, { Component } from 'react'
import GatewayForm from "./Gateway_form.jsx"
import { notification } from "antd"
import { Map, TileLayer, Marker, Popup, iconbeacon, IndoorLayer, IndoorLevelLayer } from '../../custom_leaflet/all_leaflet.js'
import ReactJson from 'react-json-view'

const openNotificationWithIcon = type => {
    notification[type]({
        message: 'Click on the map to start adding gateways("markers")',

    });
};

export default class App extends Component {

    state = {
        lat: 49.418544,
        lng: 8.676812,
        zoom: 18,
        selected_coordinates: undefined,
        added_markers: [],
        current_level: 0
    }

    componentDidMount() {
        openNotificationWithIcon('info')
    }

    mapRef = React.createRef();

    addMarker = (e) => {
        this.setState({ selected_coordinates: e.latlng })
    }

    onAddGateway = (values) => {

        this.setState((prevState) => {
            if (values) {
                values["coordinates"] = { lat: values["lat"], lng: values["lng"] }
                values["level"] = this.state.current_level
                delete values["lat"]
                delete values["lng"]
                //console.log("sucess submit!!!", values)
                prevState["added_markers"].push(values)
            }
            return {
                selected_coordinates: undefined,
                added_markers: prevState["added_markers"]
            }
        })
    }

    componentDidUpdate() {
        this.mapRef.current.leafletElement.invalidateSize();
    }

    onChangeLevel = (level) => {
        //console.log("level", level)
        this.setState({ current_level: level })
    }

    render() {
        const position = [this.state.lat, this.state.lng]

        //console.log('already added', this.state.added_markers)

        return <React.Fragment>
            <Map
                ref={this.mapRef}
                style={{ width: "100%", height: "100%" }}
                center={position}
                zoom={this.state.zoom}
                onClick={this.addMarker}
            >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <IndoorLayer no_click >
                    <IndoorLevelLayer
                        intialLevel={this.state.current_level}
                        onLevelChange={this.onChangeLevel}
                    />
                </IndoorLayer >
                {this.state.added_markers.map((item, i) => item["level"] === this.state.current_level && <Marker
                    key={i}
                    position={item["coordinates"]}
                    icon={iconbeacon}
                >
                    <Popup>
                        Gateway:<br /> {`${JSON.stringify(item)}`}
                    </Popup>
                </Marker>)}

            </Map>
            <GatewayForm
                visible={typeof this.state.selected_coordinates === 'object'}
                onSubmit={this.onAddGateway}
                initialValue={{
                    latlng: this.state.selected_coordinates
                }}
            />
            <ReactJson src={this.state.added_markers} />
        </React.Fragment>
    }
}
