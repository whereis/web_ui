import React from "react";
import { Select, Button } from "antd";
import { connect } from "react-redux";
import BuildingsActions from "../../../redux/where_is/buildings/actions";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class BeaconsPage extends React.PureComponent {
  constructor(props) {
    super(props);

    const { initialValues } = props;
    // applies initial values
    this.state = {
      selected_domain: initialValues.domain_id || undefined,
      selected_building: initialValues.building_id || undefined
    };
  }

  onSelectDomain = selected_domain => {
    this.setState({ selected_domain });
    this.props.get_buildings(selected_domain);
  };

  onSelectBuilding = selected_building => {
    this.setState({ selected_building });
    if (typeof this.props.onBuilding === "function") {
      this.props.onBuilding(this.state.selected_domain, selected_building);
    }
  };

  onRefresh = () => {
    const current_domain = this.state.selected_domain;
    const current_selected_building = this.state.selected_building;
    this.props.onBuilding(undefined, undefined);
    this.setState(
      { selected_domain: undefined, selected_building: undefined },
      () => {
        this.props.onBuilding(current_domain, current_selected_building);
        this.setState({
          selected_domain: current_domain,
          selected_building: current_selected_building
        });
      }
    );
  };

  render() {
    return (
      <React.Fragment>
        Dominio:
        <DomainsSelect
          defaultValue={this.state.selected_domain}
          onChange={this.onSelectDomain}
        />
        {this.state.selected_domain && (
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="select building"
            optionFilterProp="children"
            defaultValue={this.state.selected_building}
            onChange={this.onSelectBuilding}
            filterOption={(input, option) =>
              option.props.children
                .toLowerCase()
                .indexOf(input.toLowerCase()) !== -1
            }
          >
            {this.props.buildings_list.map((item, index) => (
              <Select.Option key={index} value={item.building_id}>
                {`${item.name}`}
              </Select.Option>
            ))}
          </Select>
        )}
        <Button
          type="primary"
          shape="circle"
          icon="redo"
          size="large"
          onClick={this.onRefresh}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    buildings_list: state.Buildings.get("buildings") || []
  }),
  {
    get_buildings: BuildingsActions.getBuildings
  }
)(BeaconsPage);
