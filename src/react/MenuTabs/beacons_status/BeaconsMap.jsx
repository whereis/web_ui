import React from "react";
import { connect } from "react-redux";
import BeaconsActions from "../../../redux/where_is/beacons/actions";
import GatewaysActions from "../../../redux/where_is/gateways/actions";
import LayoutsActions from "../../../redux/where_is/layouts/actions";
import {
  LeafletGeoTiff,
  IndoorLayer,
  IndoorLevelLayer,
  Marker,
  DriftMarker,
  Popup,
  iconbeacon,
  iconGateway,
  GatewaysPopUpContent,
  BeaconsPopUpContent
} from "../../custom_leaflet/all_leaflet.js";
import MapWrapper from "../IndoorMapTab/LeafletMap.jsx";
import { isEmpty } from "lodash";
import { Switch, Button, InputNumber, Select } from "antd";

const Option = Select.Option;

const init_state = {
  /* by building_id */ filtered_gateways: [],
  all_floors_data: {},
  floors_list: [],
  indoor_data: undefined,
  level: "0",
  show_geotiff: true,
  show_gateways: false,
  show_geojson: false,
  show_beacons: true,
  selected_beacons: [],
  interval_time: 2000
};
class BeaconsMap extends React.PureComponent {
  state = init_state;

  static getDerivedStateFromProps(props, state) {
    const add_state = {};

    if (!isEmpty(props.gateways_list) && isEmpty(state.filtered_gateways)) {
      /* gateways filtration by building id */
      const filtered_gateways = props.gateways_list.filter(
        ({ building_id }) => "" + building_id === "" + props.building_id
      );

      add_state["filtered_gateways"] = filtered_gateways;
    }

    if (!isEmpty(props.all_floors_data) && isEmpty(state.all_floors_data)) {
      add_state["all_floors_data"] = props.all_floors_data;

      const all_floors_data = props.all_floors_data;

      // set default selected floor if only 1 floor
      const layout_floors = Object.keys(all_floors_data);
      if (layout_floors.length === 1) {
        add_state["level"] = layout_floors[0];
      }

      const sim = { type: "FeatureCollection", features: [] };

      for (const key in all_floors_data) {
        const floor_data = all_floors_data[key];
        sim["features"].push(...floor_data["geojson"]["features"]);
      }

      // sets level to existing one either sets
      // auto to first floor, so when there is only one floor auto selects
      const floors_list = Object.keys(all_floors_data);
      if (floors_list.length > 0) {
        add_state["level"] = floors_list[0];
      }

      add_state["indoor_data"] = sim;
    }

    return add_state;
  }

  componentDidMount() {
    /* gets gateways */
    if (this.props.domain_id && this.props.building_id) {
      /* we have domain_id and building_id*/
      this.setState(init_state);
      this.requestBeacons();
      /* last time we didnt had the ids, so this is the first time*/
      this.props.get_gateways(this.props.domain_id);
      /* list of floors */
      this.props.getAllLayoutsFloor(
        this.props.domain_id,
        this.props.building_id
      );
      this.fetchLiveBeacons();
    }
  }

  requestBeacons = () => {
    const { domain_id, building_id } = this.props;
    if (domain_id && building_id) {
      this.props.get_beacons(domain_id, {
        building_id
      });
    }
  };

  componentDidUpdate(prevProps) {
    /* gets gateways */
    if (this.props.domain_id && this.props.building_id) {
      /* we have domain_id and building_id*/
      if (
        prevProps.domain_id === undefined &&
        prevProps.building_id === undefined
      ) {
        this.setState(init_state);
        this.requestBeacons();
        /* last time we didnt had the ids, so this is the first time*/
        this.props.get_gateways(this.props.domain_id);
        /* list of floors */
        this.props.getAllLayoutsFloor(
          this.props.domain_id,
          this.props.building_id
        );
        this.fetchLiveBeacons();
      }
    }

    // if we have beacons now and we didnt had:
    if (isEmpty(prevProps.beacons_list) && !isEmpty(this.props.beacons_list)) {
      /* gateways filtration by building id */
      const new_selected_beacons = this.props.beacons_list.map(
        ({ mac_address }) => mac_address
      );
      this.setState({
        selected_beacons: new_selected_beacons
      });
    }
  }

  fetchLiveBeacons = () => {
    this.props.get_live_beacons(this.props.domain_id, {
      building_id: this.props.building_id
    });
  };

  start_timer = () => {
    this.stop_timer();
    this.interval_id = setInterval(
      () => this.fetchLiveBeacons(),
      this.state.interval_time
    );
  };

  stop_timer = () => {
    if (this.interval_id !== undefined) {
      clearInterval(this.interval_id);
    }
  };

  componentWillUnmount() {
    this.stop_timer();
  }

  onChangeLevel = level => {
    /* saves selected level */
    this.setState({ level });
  };

  SwitchHandler = (checked, e) => {
    this.setState({
      [e.target.name]: checked
    });
  };

  onInputChange = value => {
    if (typeof value === "number") {
      this.setState({ interval_time: value });
    }
  };

  onChangeBeaconsList = value => {
    //console.log(`selected `, value);
    this.setState({
      selected_beacons: value
    });
  };

  render() {
    const {
      indoor_data,
      level,
      all_floors_data,
      filtered_gateways,
      show_geojson,
      show_beacons,
      show_gateways,
      show_geotiff,
      selected_beacons
    } = this.state;

    const geotiff = all_floors_data[level]
      ? all_floors_data[level]["geotiff"]
      : undefined;

    const marker_gateways = filtered_gateways.map(data =>
      Number(data["floor"]) === Number(level) ? (
        <Marker
          key={data["mac_address"]}
          position={data["coordinates"]}
          icon={iconGateway}
        >
          <Popup>
            <GatewaysPopUpContent data={data} />
          </Popup>
        </Marker>
      ) : null
    );

    return (
      <React.Fragment>
        {this.props.building_id && (
          <React.Fragment>
            <br />
            Beacons List:
            <Select
              mode="multiple"
              placeholder="Beacons list"
              style={{ minWidth: 150 }}
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.props.children
                  .toLowerCase()
                  .indexOf(input.toLowerCase()) >= 0
              }
              value={selected_beacons}
              onChange={this.onChangeBeaconsList}
            >
              {this.props.beacons_list.map((beacon, i) => (
                <Option key={beacon.mac_address} value={beacon.mac_address}>
                  {beacon.name}
                </Option>
              ))}
            </Select>
          </React.Fragment>
        )}

        <MapWrapper>
          {show_geotiff && geotiff && <LeafletGeoTiff tiff={geotiff} />}
          {indoor_data && (
            <IndoorLayer
              intialLevel={level}
              hideGeoJson={!show_geojson}
              data={indoor_data}
            >
              <IndoorLevelLayer
                intialLevel={level}
                onLevelChange={this.onChangeLevel}
              />
            </IndoorLayer>
          )}
          {show_gateways && marker_gateways}
          {show_beacons &&
            this.props.beacon_status_filtered.map(data =>
              Number(data["floor"]) === Number(level) &&
              selected_beacons.includes(data["mac_address"]) ? (
                <DriftMarker
                  key={data["mac_address"]}
                  position={[
                    data["locations"]["proximity"]["latitude"],
                    data["locations"]["proximity"]["longitude"]
                  ]}
                  duration={1000}
                  icon={iconbeacon}
                >
                  <Popup>
                    <BeaconsPopUpContent data={data} />
                  </Popup>
                </DriftMarker>
              ) : null
            )}
        </MapWrapper>
        {this.props.building_id && (
          <React.Fragment>
            <br />
            Geojson:{" "}
            <Switch
              checked={show_geojson}
              name="show_geojson"
              onChange={this.SwitchHandler}
            />
            <br />
            Geotiff:{" "}
            <Switch
              checked={show_geotiff}
              name="show_geotiff"
              onChange={this.SwitchHandler}
            />
            <br />
            Gateways:{" "}
            <Switch
              checked={show_gateways}
              name="show_gateways"
              onChange={this.SwitchHandler}
            />
            <br />
            Beacons :
            <Switch
              checked={show_beacons}
              name="show_beacons"
              onChange={this.SwitchHandler}
            />
            <br />
            Auto Refresh:
            <br />
            <InputNumber
              value={this.state.interval_time}
              onChange={this.onInputChange}
            />
            <br />
            <Button onClick={this.start_timer}>Start</Button>
            <Button onClick={this.stop_timer}>Stop</Button>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    beacon_status_filtered: state.Beacons.get("beacon_status") || [],
    gateways_list: state.Gateways.get("gateways") || [],
    beacons_list: state.Beacons.get("beacons") || [],
    all_floors_data: state.Layouts.get("all_floors_data") || {}
  }),
  {
    get_live_beacons: BeaconsActions.getBeaconStatus,
    get_gateways: GatewaysActions.getGateways,
    get_beacons: BeaconsActions.getBeacons,
    getAllLayoutsFloor: LayoutsActions.getAllLayoutsFloor,
    get_layout_floor: LayoutsActions.getLayoutFloor
  }
)(BeaconsMap);
