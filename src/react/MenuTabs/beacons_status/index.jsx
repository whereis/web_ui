import React from "react";
import BeaconsMap from "./BeaconsMap.jsx";
import RequestFilters from "./RequestFilters.jsx";
import { connect } from "react-redux";

class BeaconsPage extends React.PureComponent {
  constructor(props) {
    super(props);

    const query = new URLSearchParams(props.location.search),
      building_id = query.get("building_id"),
      domain_id = query.get("domain_id");
    // gets from url qury default values on mount
    if (domain_id && building_id) {
      this.state = {
        domain_id: domain_id,
        building_id: building_id
      };
    } else {
      this.state = {
        domain_id: undefined,
        building_id: undefined
      };
    }
  }

  render() {
    return (
      <React.Fragment>
        <RequestFilters
          initialValues={{
            domain_id: this.state.domain_id,
            building_id: this.state.building_id
          }}
          onBuilding={(domain_id, building_id) =>
            this.setState({ domain_id, building_id })
          }
        />
        <BeaconsMap
          key={Date.now()}
          domain_id={this.state.domain_id}
          building_id={this.state.building_id}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({}),
  {}
)(BeaconsPage);
