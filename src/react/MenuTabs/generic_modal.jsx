import React from "react";
import { Button, Modal } from "antd";

class GenericForm extends React.PureComponent {
  state = {
    visible: false
  };

  render() {
    return (
      <React.Fragment>
        <Button type="primary" onClick={() => this.setState({ visible: true })}>
          Adicionar
        </Button>
        <Modal
          visible={this.state.visible}
          style={{ top: 10 }}
          title="Adicionar"
          onCancel={() => this.setState({ visible: false })}
          footer={[
            <Button key="submit" type="primary" onClick={this.props.onOK}>
              Adicionar
            </Button>
          ]}
        >
          {this.props.children}
        </Modal>
        <br />
      </React.Fragment>
    );
  }
}

export default GenericForm;
