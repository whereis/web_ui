import React, { Component } from "react";
import {
  iconbeacon,
  Popup,
  Tooltip,
  DriftMarker
} from "../../custom_leaflet/all_leaflet.js";
import gen_latlng from "./generate_position.js";

export default class MarkerManager extends Component {
  constructor(props) {
    super(props);

    let new_markers = [];
    if (props.markers_amount) {
      new_markers = this.gen_markers(props.markers_amount);
    }

    this.state = {
      markers: new_markers
    };
    this.request_new_markers.bind(this);
  }

  componentDidMount() {
    this.request_new_markers();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.refresh_sec !== this.props.refresh_sec &&
      this.props.refresh_sec
    ) {
      this.request_new_markers();
    }
    if (
      prevProps.markers_amount !== this.props.markers_amount &&
      this.props.markers_amount
    ) {
      // updates number of markers
      this.setState(
        {
          markers: this.gen_markers(this.props.markers_amount)
        },
        this.request_new_markers
      );
    }
  }

  request_new_markers = () => {
    if (this.anime_frame || this.timer) {
      cancelAnimationFrame(this.anime_frame);
      clearTimeout(this.timer);
    }

    this.anime_frame = requestAnimationFrame(this.go);
  };

  go = () => {
    this.timer = setTimeout(() => {
      this.setState(
        state => ({
          markers: state.markers.map(marker => ({
            name: marker["name"],
            latlng: gen_latlng()
          }))
        }),
        this.request_new_markers
      );
    }, this.props.refresh_sec || 5000);
  };

  gen_markers(markers_amount) {
    const new_markers = [];
    for (let index = 0; index < markers_amount; index++) {
      new_markers.push({
        name: "marker" + index,
        latlng: gen_latlng()
      });
    }
    return new_markers;
  }

  render() {
    //console.log("slining markers=", this.state.markers.length);

    return this.state.markers.map(marker => (
      <DriftMarker
        key={marker["name"]}
        position={marker["latlng"]}
        duration={1000}
        zIndexOffset={2} //makes bus icon over stops icon
        icon={iconbeacon}
      >
        <Popup>
          <div>
            sim
            {`s: ${marker["name"]}`}
          </div>
        </Popup>
        <Tooltip>Test</Tooltip>
      </DriftMarker>
    ));
  }
}
