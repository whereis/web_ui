import React, { Component } from 'react'
import { Map, TileLayer, IndoorLayer, IndoorLevelLayer } from '../../custom_leaflet/all_leaflet.js'
import MarkerManager from "./MarkerManager.jsx"
import MarkersFormSettings from "./MarkersFormSettings.jsx"

export default class App extends Component {

    state = {
        lat: 49.418544,
        lng: 8.676812,
        zoom: 18,
        current_level: 0,
        markers_amount: 1,
        refresh_sec: 1000
    }

    mapRef = React.createRef();

    componentDidUpdate() {
        this.mapRef.current.leafletElement.invalidateSize();
    }

    onChangeLevel = (level) => {
        //console.log("level changed", level)
        this.setState({ current_level: level })
    }

    render() {
        const position = [this.state.lat, this.state.lng]

        return <React.Fragment>
            <MarkersFormSettings onSubmit={(body) => this.setState(body)} />
            <Map
                ref={this.mapRef}
                style={{ width: "100%", height: "100%" }}
                center={position}
                zoom={this.state.zoom}
                onClick={this.addMarker}
            >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <IndoorLayer >
                    <IndoorLevelLayer
                        intialLevel={this.state.current_level}
                        onLevelChange={this.onChangeLevel}
                    />
                </IndoorLayer >
                <MarkerManager {...this.state} />
            </Map>
        </React.Fragment>
    }
}
