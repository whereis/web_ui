import React from "react"
import SampleMap from "./IndoorMapTab/SampleMap"
import AddLevels from "./AddLevels/index.jsx"
import Domains from "./domains/index.jsx"
import Venues from "./venues/index.jsx"
import Buildings from "./buildings/index.jsx"
import Layouts from "./layouts/index.jsx"
import Gateways from "./gateways/index.jsx"
import Beacons from "./beacons/index.jsx"
import BeaconsStatus from "./beacons_status/index.jsx"
import Overview from "./overview/index.jsx"

import TestGateways from "./TestGateways/index.jsx"
/* live tests */
import LiveSlidingMapTest from "./LiveSlidingMapTest/index.jsx"
import LiveMapMarkers from "./LiveMapMarkers/index.jsx"
import LiveMapCanvasMarkers from "./LiveMapCanvasMarkers/index.jsx"

const EmptyComp = () => <div>empty</div>

export {
    SampleMap,
    AddLevels,
    EmptyComp,
    Domains,
    Venues,
    Buildings,
    Layouts,
    Gateways,
    Beacons,
    BeaconsStatus,
    Overview,

    TestGateways,
    LiveSlidingMapTest,
    LiveMapMarkers,
    LiveMapCanvasMarkers
}