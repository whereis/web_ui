import React from "react";
import { Card, Col, Row, Button, Icon } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import BuildingsActions from "../../../redux/where_is/buildings/actions";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class OverviewPage extends React.PureComponent {
  state = {
    selected_domain: undefined
  };

  onSelectDomain = selected_domain => {
    this.setState({ selected_domain });
    this.props.get_buildings(selected_domain);
  };

  onRefresh = () => {
    const current_domain = this.state.selected_domain;
    this.setState({ selected_domain: undefined }, () =>
      this.onSelectDomain(current_domain)
    );
  };

  render() {
    return (
      <React.Fragment>
        Dominio:
        <DomainsSelect onChange={this.onSelectDomain} />
        <Button
          type="primary"
          shape="circle"
          icon="redo"
          size="large"
          onClick={this.onRefresh}
        />
        <Row gutter={16}>
          {this.props.buildings_list.map(building => {
            return (
              <Col span={12} key={`${building.building_id}`}>
                <Card
                  extra={
                    <Link
                      to={`/asset_location?domain_id=${this.state.selected_domain}&building_id=${building.building_id}`}
                    >
                      Live
                    </Link>
                  }
                  title={
                    <span>
                      <Icon type="home" />
                      &nbsp;
                      {building.name}
                    </span>
                  }
                  bordered={false}
                >
                  <p>{building.address}</p>
                  <p>{building.description}</p>
                </Card>
              </Col>
            );
          })}
        </Row>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    buildings_list: state.Buildings.get("buildings") || []
  }),
  {
    get_buildings: BuildingsActions.getBuildings
  }
)(OverviewPage);
