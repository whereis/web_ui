import React from "react"
import { connect } from "react-redux";
import DomainsActions from "../../../redux/where_is/domains/actions"
import { Form, Input } from 'antd';
import GenericForm from "../generic_modal.jsx"
class DomainForm extends React.PureComponent {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, {
            name,
            description
        }) => {
            if (!err) {
                //console.log('Received values of form: ', name, description);
                this.props.add_request(name, description)
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        return <GenericForm onOK={this.handleSubmit}>
            <Form >
                <Form.Item>
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Please input domain name!' }],
                    })(<Input placeholder="Name" />)}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('description', {
                        rules: [{ required: true, message: 'Please input a description!' }],
                    })(<Input placeholder="Description" />,
                    )}
                </Form.Item>
            </Form>
        </GenericForm>

    }
}


export default connect(null,
    { add_request: DomainsActions.addDomain, },
)(Form.create()(DomainForm));
