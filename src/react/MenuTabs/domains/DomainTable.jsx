import React from "react"
import { connect } from "react-redux";
import DomainsActions from "../../../redux/where_is/domains/actions"
import { Table, Button } from 'antd';

class DomainsTable extends React.PureComponent {

    componentDidMount() {
        this.props.get_request()
    }

    generate_columns = () => [
        {
            title: 'id',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'provisioned_on',
            key: 'provisioned_on',
            dataIndex: 'provisioned_on',
        },
        {
            title: 'updated_on',
            key: 'updated_on',
            dataIndex: 'updated_on',
        },
        {
            title: 'Action',
            key: 'action',
            render: (record) => <Button
                onClick={() => this.props.delete_by_id_request(record["id"])}  >
                Delete
                </Button>,
        }
    ]


    render() {

        return <Table
            columns={this.generate_columns()}
            rowKey={(r, i) => i}
            dataSource={this.props.data} />
    }
}

export default connect(state => ({
    data: state.Domains.get('domains') || [],
}),
    {
        get_request: DomainsActions.getDomains,
        delete_by_id_request: DomainsActions.deleteDomain,
    },
)(DomainsTable);