import React from "react"
import AddDomainForm from "./AddDomain.jsx"
import DomainsTable from "./DomainTable.jsx"

class DomainPage extends React.PureComponent {

    render() {

        return <React.Fragment>
            <AddDomainForm />
            <DomainsTable />
        </React.Fragment>
    }
}


export default DomainPage