import React from "react";
import { connect } from "react-redux";
import BeaconsActions from "../../../../redux/where_is/beacons/actions";
import { Table } from "antd";
import TableActionButtons from "./ActionButtons/TableActionButtons";

class BeaconsTable extends React.PureComponent {
  componentDidMount() {
    if (this.props.domain_id) {
      this.props.get_request(this.props.domain_id);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.domain_id && this.props.domain_id !== prevProps.domain_id) {
      this.props.get_request(this.props.domain_id);
    }
  }

  generate_columns = () => [
    {
      title: "venue_id",
      dataIndex: "venue_id",
      key: "venue_id"
    },
    {
      title: "mac_address",
      dataIndex: "mac_address",
      key: "mac_address"
    },
    {
      title: "equipment_id",
      dataIndex: "equipment_id",
      key: "equipment_id"
    },
    {
      title: "name",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "manufactor",
      dataIndex: "manufactor",
      key: "manufactor"
    },
    {
      title: "model",
      dataIndex: "model",
      key: "model"
    },
    {
      title: "status",
      dataIndex: "status",
      key: "status"
    },
    {
      title: "provisioned_on",
      key: "provisioned_on",
      dataIndex: "provisioned_on"
    },
    {
      title: "updated_on",
      key: "updated_on",
      dataIndex: "updated_on"
    },
    {
      title: "Action",
      key: "action",
      render: record => (
        <TableActionButtons
          delete_by_id_request={this.props.delete_by_id_request}
          data={record}
          domain_id={this.props.domain_id}
        />
      )
    }
  ];

  /**
   * Function that does the local search of the data
   * @param {Sptring} searchText text to search throughout the columns of the table
   * @return {Array} data filtered by the search text
   */
  onSearch(data, searchText) {
    return searchText && data.length > 0
      ? data.filter(data => {
          const text = searchText.toUpperCase();
          //search specific columns
          const filter_columns = ["mac_address", "name", "manufactor"];
          for (let index = 0; index < filter_columns.length; index++) {
            const column_name = filter_columns[index];
            if (
              data[column_name] &&
              data[column_name]
                .toString()
                .toUpperCase()
                .includes(text)
            ) {
              return true;
            }
          }
          return false;
        })
      : data;
  }

  render() {
    const { data, search_text } = this.props;

    return (
      <Table
        columns={this.generate_columns()}
        rowKey={(r, i) => i}
        dataSource={this.onSearch(data, search_text)}
      />
    );
  }
}

export default connect(
  (state, ownProps) => ({
    data: ownProps.domain_id ? state.Beacons.get("beacons") || [] : [] // check to require domain_id input, qhen user comes from other pages
  }),
  {
    get_request: BeaconsActions.getBeacons,
    delete_by_id_request: BeaconsActions.deleteBeacon
  }
)(BeaconsTable);
