import React from "react";
import { Button } from "antd";
import PreviewModal from "./PreviewModal.jsx";

class TableActionButtons extends React.PureComponent {
  render() {
    const { data, domain_id, delete_by_id_request } = this.props;

    return (
      <React.Fragment>
        <PreviewModal 
        building_id={data["building_id"]}
        mac_address={data["mac_address"]}
        domain_id={domain_id}
        />
        <Button
          onClick={() => delete_by_id_request(domain_id, data["equipment_id"])}
        >
          Delete
        </Button>
      </React.Fragment>
    );
  }
}

export default TableActionButtons;
