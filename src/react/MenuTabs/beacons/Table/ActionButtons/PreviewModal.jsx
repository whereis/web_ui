import React from "react";
import { Modal, Button, Switch, InputNumber } from "antd";
import MapWrapper from "../../../IndoorMapTab/LeafletMap.jsx";
import {
  LeafletGeoTiff,
  IndoorLayer,
  IndoorLevelLayer,
  Marker,
  DriftMarker,
  Popup,
  iconbeacon,
  iconGateway,
  GatewaysPopUpContent,
  BeaconsPopUpContent
} from "../../../../custom_leaflet/all_leaflet.js";
import { connect } from "react-redux";
import BeaconsActions from "../../../../../redux/where_is/beacons/actions";
import GatewaysActions from "../../../../../redux/where_is/gateways/actions";
import LayoutsActions from "../../../../../redux/where_is/layouts/actions";
import { isEmpty } from "lodash";

const init_state = {
  filtered_gateways: [],
  all_floors_data: {},
  floors_list: [],
  indoor_data: undefined,
  level: undefined, // waits for beacon fetch to define floor
  show_geotiff: true,
  show_gateways: false,
  show_geojson: false,
  show_beacons: true,
  interval_time: 2000
};

class PreviewModal extends React.PureComponent {
  state = {
    ...init_state,
    visible: false
  };

  static getDerivedStateFromProps(props, state) {
    const add_state = {};

    if (!isEmpty(props.gateways_list) && isEmpty(state.filtered_gateways)) {
      /* gateways filtration by building id */
      const filtered_gateways = props.gateways_list.filter(
        ({ building_id }) => "" + building_id === "" + props.building_id
      );

      add_state["filtered_gateways"] = filtered_gateways;
    }

    if (!isEmpty(props.all_floors_data) && isEmpty(state.all_floors_data)) {
      add_state["all_floors_data"] = props.all_floors_data;

      const all_floors_data = props.all_floors_data;

      const sim = { type: "FeatureCollection", features: [] };

      for (const key in all_floors_data) {
        const floor_data = all_floors_data[key];
        sim["features"].push(...floor_data["geojson"]["features"]);
      }

      add_state["indoor_data"] = sim;
    }

    if (
      Array.isArray(props.beacon_status_filtered) &&
      props.beacon_status_filtered.length === 1
    ) {
      // if there is only 1 beacon auto selects level to match beacons level
      const beacon_floor = `${props.beacon_status_filtered[0].floor}`;

      if (state.level !== beacon_floor) {
        // in case state level is diffrent from beacons set it to match
        add_state["level"] = beacon_floor;
      }
    }

    return add_state;
  }

  onPreviewClick = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  onChangeLevel = level => {
    /* saves selected level */
    this.setState({ level });
  };

  SwitchHandler = (checked, e) => {
    this.setState({
      [e.target.name]: checked
    });
  };

  onInputChange = value => {
    if (typeof value === "number") {
      this.setState({ interval_time: value });
    }
  };

  fetchLiveBeacons = () => {
    this.props.get_live_beacons(this.props.domain_id, {
      mac_address: this.props.mac_address
    });
  };

  start_timer = () => {
    this.stop_timer();
    this.interval_id = setInterval(
      () => this.fetchLiveBeacons(),
      this.state.interval_time
    );
  };

  stop_timer = () => {
    if (this.interval_id !== undefined) {
      clearInterval(this.interval_id);
    }
  };

  componentWillUnmount() {
    this.stop_timer();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.visible === false && this.state.visible) {
      //modal was open now
      this.setState(init_state);
      /* last time we didnt had the ids, so this is the first time*/
      this.props.get_gateways(this.props.domain_id);
      /* list of floors */
      this.props.getAllLayoutsFloor(
        this.props.domain_id,
        this.props.building_id
      );
      this.fetchLiveBeacons();
    }
    if (prevState.visible && this.state.visible === false) {
      this.stop_timer();
    }
  }

  render() {
    const {
      indoor_data,
      level,
      all_floors_data,
      filtered_gateways,
      show_geojson,
      show_beacons,
      show_gateways,
      show_geotiff
    } = this.state;

    const geotiff = all_floors_data[level]
      ? all_floors_data[level]["geotiff"]
      : undefined;

    const marker_gateways = filtered_gateways.map(data =>
      Number(data["floor"]) === Number(level) ? (
        <Marker
          key={data["mac_address"]}
          position={data["coordinates"]}
          icon={iconGateway}
        >
          <Popup>
            <GatewaysPopUpContent data={data} />
          </Popup>
        </Marker>
      ) : null
    );

    return (
      <React.Fragment>
        <Button onClick={this.onPreviewClick}>Live</Button>
        <Modal
          style={{ top: 10 }}
          visible={this.state.visible}
          title="Preview Asset"
          onCancel={this.handleCancel}
          forceRender
          footer={[]}
        >
          <div style={{ height: 500 }}>
            <MapWrapper>
              {show_geotiff && geotiff && <LeafletGeoTiff tiff={geotiff} />}
              {indoor_data && level && (
                <IndoorLayer
                  hideGeoJson={!show_geojson}
                  intialLevel={level}
                  data={indoor_data}
                >
                  <IndoorLevelLayer
                    intialLevel={level}
                    onLevelChange={this.onChangeLevel}
                  />
                </IndoorLayer>
              )}
              {show_gateways && marker_gateways}
              {show_beacons &&
                this.props.beacon_status_filtered.map(data =>
                  Number(data["floor"]) === Number(level) ? (
                    <DriftMarker
                      key={data["mac_address"]}
                      position={[
                        data["locations"]["proximity"]["latitude"],
                        data["locations"]["proximity"]["longitude"]
                      ]}
                      duration={1000}
                      icon={iconbeacon}
                    >
                      <Popup>
                        <BeaconsPopUpContent data={data} />
                      </Popup>
                    </DriftMarker>
                  ) : null
                )}
            </MapWrapper>
          </div>
          <React.Fragment>
            <br />
            Geojson:{" "}
            <Switch
              checked={show_geojson}
              name="show_geojson"
              onChange={this.SwitchHandler}
            />
            <br />
            Geotiff:{" "}
            <Switch
              checked={show_geotiff}
              name="show_geotiff"
              onChange={this.SwitchHandler}
            />
            <br />
            Gateways:{" "}
            <Switch
              checked={show_gateways}
              name="show_gateways"
              onChange={this.SwitchHandler}
            />
            <br />
            Beacons :
            <Switch
              checked={show_beacons}
              name="show_beacons"
              onChange={this.SwitchHandler}
            />
            <br />
            Auto Refresh:
            <br />
            <InputNumber
              value={this.state.interval_time}
              onChange={this.onInputChange}
            />
            <br />
            <Button onClick={this.start_timer}>Start</Button>
            <Button onClick={this.stop_timer}>Stop</Button>
          </React.Fragment>
        </Modal>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    beacon_status_filtered: state.Beacons.get("beacon_status") || [],
    gateways_list: state.Gateways.get("gateways") || [],
    layouts_list: state.Layouts.get("layouts") || [],
    all_floors_data: state.Layouts.get("all_floors_data") || {}
  }),
  {
    get_live_beacons: BeaconsActions.getBeaconStatus,
    get_gateways: GatewaysActions.getGateways,
    getAllLayoutsFloor: LayoutsActions.getAllLayoutsFloor,
    get_layout_floor: LayoutsActions.getLayoutFloor
  }
)(PreviewModal);
