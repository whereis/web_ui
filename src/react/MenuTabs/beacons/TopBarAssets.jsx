import React from "react";
import {  Button, Row, Col, Input } from "antd";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

const { Search } = Input;

class BeaconsPage extends React.PureComponent {
  SearchMiddleMan = text => {
    if (text.length === 0) {
      this.props.onSearch(undefined);
    } else {
      this.props.onSearch(text);
    }
  };

  render() {
    const { onSelectDomain, onRefresh } = this.props;
    return (
      <Row>
        <Col xs={24} sm={12}>
          Dominio:
          <DomainsSelect onChange={onSelectDomain} />
          <Button
            type="primary"
            shape="circle"
            icon="redo"
            size="large"
            onClick={onRefresh}
          />
        </Col>
        <Col
          xs={24}
          sm={12}
          style={{
            textAlign: "right"
          }}
        >
          <Search
            style={{
              maxWidth: 200
            }}
            placeholder="Search"
            onSearch={this.SearchMiddleMan}
            enterButton
          />
        </Col>
      </Row>
    );
  }
}

export default BeaconsPage;
