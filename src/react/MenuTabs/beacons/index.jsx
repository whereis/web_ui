import React from "react";
import AddBeaconForm from "./AddBeacon.jsx";
import BeaconsTable from "./Table/BeaconsTable.jsx";
import TopBarAssets from "./TopBarAssets";

const init_state = {
  selected_domain: undefined,
  search_text: undefined
};

export default class BeaconsPage extends React.PureComponent {
  state = init_state;

  onSelectDomain = selected_domain => {
    this.setState({ selected_domain });
  };

  onSearch = search_text => {
    this.setState({ search_text });
  };

  onRefresh = () => {
    const current_domain = this.state.selected_domain;
    // const search_text = this.state.search_text;
    this.setState(init_state, () =>
      this.setState({ selected_domain: current_domain })
    );
  };

  render() {
    return (
      <React.Fragment>
        <AddBeaconForm />
        <TopBarAssets
          onSelectDomain={this.onSelectDomain}
          onSearch={this.onSearch}
          onRefresh={this.onRefresh}
        />
        <BeaconsTable
          search_text={this.state.search_text}
          domain_id={this.state.selected_domain}
        />
      </React.Fragment>
    );
  }
}
