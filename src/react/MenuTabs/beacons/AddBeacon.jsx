import React from "react";
import { connect } from "react-redux";
import BeaconsActions from "../../../redux/where_is/beacons/actions";
import VenuesActions from "../../../redux/where_is/venues/actions";
import BuildingsActions from "../../../redux/where_is/buildings/actions";
import { Form, Input, Select } from "antd";
import GenericForm from "../generic_modal.jsx";
import StatusSelect from "../../ReusableComponents/StatusSelect.jsx";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class BeaconForm extends React.PureComponent {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        //console.log("Received values of form: ", values);
        const domain_id = values.domain_id;
        delete values.domain_id;
        this.props.add_request(domain_id, values);
      }
    });
  };

  get_with_domain = id => {
    this.props.get_venues_by_domain(id);
    this.props.get_buildings_by_domain(id);
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <GenericForm onOK={this.handleSubmit}>
        <Form>
          <Form.Item label="domain">
            {getFieldDecorator("domain_id", {
              rules: [{ required: true, message: "Please select domain" }]
            })(<DomainsSelect onChange={this.get_with_domain} />)}
          </Form.Item>
          <Form.Item label="venue">
            {getFieldDecorator("venue_id", {
              rules: [{ required: true, message: "Please select venue" }]
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select venue"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) !== -1
                }
              >
                {this.props.venues_list.map((item, index) => (
                  <Select.Option key={index} value={item.id}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item label="building">
            {getFieldDecorator("building_id", {
              rules: [{ required: true, message: "Please select building" }]
            })(
              <Select
                showSearch
                style={{ width: 200 }}
                placeholder="Select venue"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children
                    .toLowerCase()
                    .indexOf(input.toLowerCase()) !== -1
                }
              >
                {this.props.buildings_list.map((item, index) => (
                  <Select.Option key={index} value={item.building_id}>
                    {item.name}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("name", {
              rules: [{ required: true, message: "Please input name!" }]
            })(<Input placeholder="Name" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("mac_address", {
              rules: [
                { required: true, message: "Please input a mac_address!" }
              ]
            })(<Input placeholder="mac_address" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("configuration", {
              rules: [
                { required: true, message: "Please input a configuration!" }
              ]
            })(<Input placeholder="configuration" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("manufactor", {
              rules: [{ required: true, message: "Please input a manufactor!" }]
            })(<Input placeholder="manufactor" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("model", {
              rules: [{ required: true, message: "Please input a model!" }]
            })(<Input placeholder="model" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("status", {
              rules: [{ required: true, message: "Please input a status!" }]
            })(<StatusSelect />)}
          </Form.Item>
        </Form>
      </GenericForm>
    );
  }
}

export default connect(
  state => ({
    domains_list: state.Domains.get("domains") || [],
    venues_list: state.Venues.get("venues") || [],
    buildings_list: state.Buildings.get("buildings") || []
  }),
  {
    add_request: BeaconsActions.addBeacon,
    get_venues_by_domain: VenuesActions.getVenue,
    get_buildings_by_domain: BuildingsActions.getBuildings
  }
)(Form.create()(BeaconForm));
