import React from "react";
import { Button } from "antd";
import AddVenueForm from "./AddVenue.jsx";
import VenuesTable from "./VenuesTable.jsx";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class VenuePage extends React.PureComponent {
  state = {
    selected_domain: undefined
  };

  onSelectDomain = selected_domain => {
    this.setState({ selected_domain });
  };

  onRefresh = () => {
    const current_domain = this.state.selected_domain;
    this.setState({ selected_domain: undefined }, () =>
      this.setState({ selected_domain: current_domain })
    );
  };

  render() {
    return (
      <React.Fragment>
        <AddVenueForm />
        <DomainsSelect onChange={this.onSelectDomain} />
        <Button
          type="primary"
          shape="circle"
          icon="redo"
          size="large"
          onClick={this.onRefresh}
        />
        <VenuesTable domain_id={this.state.selected_domain} />
      </React.Fragment>
    );
  }
}

export default VenuePage;
