import React from "react";
import { connect } from "react-redux";
import VenuesActions from "../../../redux/where_is/venues/actions";
import { Form, Input } from "antd";
import GenericForm from "../generic_modal.jsx";
import StatusSelect from "../../ReusableComponents/StatusSelect.jsx";
import DomainsSelect from "../../ReusableComponents/DomainsSelect";

class VenueForm extends React.PureComponent {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(
      (err, { domain_id, name, description, status }) => {
        if (!err) {
          //console.log("Received values of form: ", name, description);
          this.props.add_request(domain_id, name, description, status);
        }
      }
    );
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <GenericForm onOK={this.handleSubmit}>
        <Form>
          <Form.Item>
            {getFieldDecorator("domain_id", {
              rules: [{ required: true, message: "Please select domain" }]
            })(<DomainsSelect />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("name", {
              rules: [{ required: true, message: "Please input domain name!" }]
            })(<Input placeholder="Name" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator("description", {
              rules: [
                { required: true, message: "Please input a description!" }
              ]
            })(<Input placeholder="Description" />)}
          </Form.Item>
          <Form.Item label="Status">
            {getFieldDecorator("status")(<StatusSelect />)}
          </Form.Item>
        </Form>
      </GenericForm>
    );
  }
}

export default connect(
  state => ({
    domains: state.Domains.get("domains") || [
      {
        id: "269a9d39-8680-4823-a5f1-2b38cbb8fd46",
        name: "Wavecom",
        description: "Domain description",
        provisioned_on: "2019-07-13T15:23:49.404Z",
        updated_on: "2019-07-13T15:23:49.404Z"
      }
    ]
  }),
  {
    add_request: VenuesActions.addVenue
  }
)(Form.create()(VenueForm));
