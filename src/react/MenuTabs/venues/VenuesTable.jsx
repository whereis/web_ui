import React from "react"
import { connect } from "react-redux";
import VenuesActions from "../../../redux/where_is/venues/actions"
import { Table, Button } from 'antd';

class VenuesTable extends React.PureComponent {

    componentDidMount() {
        if (this.props.domain_id) {
            this.props.get_request(this.props.domain_id)
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.domain_id && this.props.domain_id !== prevProps.domain_id) {
            this.props.get_request(this.props.domain_id)
        }
    }

    generate_columns = () => [
        {
            title: 'id',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'domain_id',
            dataIndex: 'domain_id',
            key: 'domain_id',
        },
        {
            title: 'name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'provisioned_on',
            key: 'provisioned_on',
            dataIndex: 'provisioned_on',
        },
        {
            title: 'updated_on',
            key: 'updated_on',
            dataIndex: 'updated_on',
        },
        {
            title: 'Action',
            key: 'action',
            render: (record) => <Button
                onClick={() => this.props.delete_by_id_request(this.props.domain_id, record["id"])}  >
                Delete
                </Button>,
        }
    ]


    render() {

        return <Table
            columns={this.generate_columns()}
            rowKey={(r, i) => i}
            dataSource={this.props.data} />
    }
}

export default connect(state => ({
    data: state.Venues.get('venues') || [],
}),
    {
        get_request: VenuesActions.getVenue,
        delete_by_id_request: VenuesActions.deleteVenue,
    },
)(VenuesTable);