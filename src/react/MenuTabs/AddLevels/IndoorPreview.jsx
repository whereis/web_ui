import React, { Component } from 'react'
import {
    Map, TileLayer, IndoorLayer, IndoorLevelLayer, L, LeafletGeoTiff
} from "../../custom_leaflet/all_leaflet.js"

export default class App extends Component {
    state = {
        lat: 49.418544,
        lng: 8.676812,
        zoom: 18,
        geo: undefined,
        current_level: "0"
    }

    mapRef = React.createRef();

    componentDidMount() {
        const jsonLayer = L.geoJson(this.props.data)
        // focus on tiff
        this.mapRef.current.leafletElement.fitBounds(jsonLayer.getBounds());
        //console.log("bounds mount", jsonLayer.getBounds())
    }

    componentDidUpdate(prevProps) {
        this.mapRef.current.leafletElement.invalidateSize();
        if (this.props.data && this.props.data !== prevProps.data) {
            const jsonLayer = L.geoJson(this.props.data)
            // focus on tiff
            this.mapRef.current.leafletElement.fitBounds(jsonLayer.getBounds());
        }
    }

    render() {
        const position = [this.state.lat, this.state.lng]
        //console.log("this.props.tiffs", this.props.tiffs)
        const tiff = this.props.tiffs[this.state.current_level]

        return <React.Fragment>
            <Map ref={this.mapRef} style={{ width: "100%", height: "100%" }} center={position} zoom={this.state.zoom}>
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <IndoorLayer data={this.props.data}>
                    <IndoorLevelLayer onLevelChange={(level) => this.setState({ current_level: `${level}` })} />
                    <LeafletGeoTiff tiff={tiff} />
                </IndoorLayer >
            </Map>
        </React.Fragment>
    }
}
