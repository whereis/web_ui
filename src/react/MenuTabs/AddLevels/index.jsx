
import React from 'react';
import CountLevelsForm from "./CountLevelsForm.jsx"
import EachLevel from "./EachLevel.jsx"
import IndoorPreview from "./IndoorPreview.jsx"

class MainSampleTab extends React.Component {
    state = {
        levels_count: undefined
    };

    prepareGeojson = (geojson, tiff_per_level) => {
        //console.log("noew it goes", geojson)

        const indoor_transformed = {
            "type": "FeatureCollection",
            "features": []
        }
        // iterates over levels
        for (let index = 0; index < geojson.length; index++) {
            const current_level = geojson[index]
            // iterates over features
            for (let findex = 0; findex < current_level.length; findex++) {
                const current_feature = current_level[findex]

                current_feature["id"] = `way/${index}${findex}`
                current_feature["properties"] = {
                    "type": "way",
                    "id": `${index}${findex}`,
                    "tags": {
                        "buildingpart": "hall",
                        "height": "3.9",
                        "name": "012"
                    },
                    "relations": [
                        {
                            "role": "buildingpart",
                            "rel": "1370728",
                            "reltags": {
                                "height": "4",
                                "level": `${index}`,
                                "name": "Erdgeschoss",
                                "type": "level"
                            }
                        }
                    ],
                    "meta": {}
                }

                indoor_transformed.features.push(current_feature)
            }
        }

        /* {
                    "type": "Feature",
                    "id": "way/94551277",
                    "properties": {
                        "type": "way",
                        "id": "94551277",
                        "tags": {
                            "buildingpart": "hall",
                            "height": "3.9",
                            "name": "012"
                        },
                        "relations": [
                            {
                                "role": "buildingpart",
                                "rel": "1370728",
                                "reltags": {
                                    "height": "4",
                                    "level": "0",
                                    "name": "Erdgeschoss",
                                    "type": "level"
                                }
                            }
                        ],
                        "meta": {}
                    },
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": [
                            [
                                [
                                    8.6770429,
                                    49.4186016
                                ],
                                [
                                    8.677043,
                                    49.418499
                                ],
                                [
                                    8.6771835,
                                    49.418499
                                ],
                                [
                                    8.6771835,
                                    49.4186016
                                ],
                                [
                                    8.6770429,
                                    49.4186016
                                ]
                            ]
                        ]
                    }
                } */
        this.setState({ indoor_transformed, tiff_per_level })
    }

    render() {

        return <div style={{ width: "100%", height: "100%" }}>
            {this.state.levels_count ?
                this.state.indoor_transformed ?
                    <IndoorPreview data={this.state.indoor_transformed} tiffs={this.state.tiff_per_level} />
                    :
                    <EachLevel
                        number_of_levels={this.state.levels_count}
                        onSave={this.prepareGeojson} />
                :
                <CountLevelsForm onSave={(levels_count) => this.setState({ levels_count })} />
            }

        </div>
    }
}


export default MainSampleTab
