import React from "react"
import { Steps, Button, message } from 'antd';
import DrawExport from "./DrawExport.jsx"

const { Step } = Steps;

export default class LevelSteps extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current: 0,
            each_level: [],
            each_level_tiff: {}
        };
    }

    next = () => {
        const current = this.state.current + 1;
        if (current === this.props["number_of_levels"]) {
            this.props.onSave(this.state.each_level, this.state.each_level_tiff)
        } else {
            this.setState({ current });
        }

    }

    prev = () => {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    onSaveLevel = ({ features }, index, tiff) => {


        this.setState(({ each_level,each_level_tiff }) => {
            if (each_level[index]) {
                each_level[index] = features
            } else {
                each_level.push(features)
            }

            each_level_tiff[index] = tiff

            return {
                each_level,
                each_level_tiff
            }
        }, this.next)

    }

    render() {
        const { current } = this.state, steps = []

        for (let index = 0; index < this.props["number_of_levels"]; index++) {

            steps.push(
                {
                    title: `${index} Level`,
                }
            )
        }

        steps.push(
            {
                title: `Last Step`,
            }
        )

        return (
            <React.Fragment>
                <Steps current={current}>
                    {steps.map(item => <Step key={item.title} title={item.title} />)}
                </Steps>
                <DrawExport index={current} onSave={this.onSaveLevel} />
                <div >
                    {current === steps.length - 1 && (
                        <Button type="primary" onClick={() => message.success('Processing complete!')}>
                            Done
            </Button>
                    )}
                    {/* current > 0 && (
                        <Button style={{ marginLeft: 8 }} onClick={this.prev}>
                            Previous
                        </Button>) */}
                </div>
            </React.Fragment>
        );
    }
}