
import React from 'react';
import Uploader from "./Upload.jsx"
import MapWrapper from "../IndoorMapTab/LeafletMap.jsx"
import { LeafletGeoTiff, FeatureGroup, EditControl, simplestyle, L, bindPopup } from "../../custom_leaflet/all_leaflet.js"
import { Button } from "antd"

class MainSampleTab extends React.Component {
    state = {
        uploaded_tiff: undefined,
        geo: undefined
    };

    saveRef = (ref) => { this.FeatureGroupRef = ref }

    onDrawStop = (event) => {
        const { layer } = event
        //console.log('event :', event);
        //console.log('event draw :', event);
        //event.layer.bindPopup('Hello');
        const geojson = layer.toGeoJSON()
        L.geoJson(geojson, {
            style: simplestyle.style,
            pointToLayer: function (feature, latlon) {
                if (!feature.properties) feature.properties = {};
                return simplestyle.marker_style(feature, latlon);
            }
        }).eachLayer(
            (l) => {
                bindPopup(l, true)
                l.addTo(this.FeatureGroupRef.leafletElement);
            }
        );
    }

    componentDidUpdate(prevProps) {
        if (prevProps["index"] !== this.props.index) {
            //resets state
            this.setState({
                uploaded_tiff: undefined,
                geo: undefined
            })
        }
    }

    onSavejson = () => {
        const a = this.FeatureGroupRef.leafletElement.toGeoJSON()
        //console.log(a)
        //layer geojson, current index(level), current tiff(probably in base 64)
        this.props.onSave(a, this.props.index, this.state.uploaded_tiff)
    }

    render() {
        const { uploaded_tiff } = this.state

        return <div style={{ width: "100%", height: "100%" }}>
            <div style={{ width: "100%", height: "80%" }}>
                {!uploaded_tiff ?
                    <Uploader onUpload={(uploaded_tiff) => this.setState({ uploaded_tiff })} />
                    : <MapWrapper >
                        <LeafletGeoTiff tiff={uploaded_tiff} />
                        <FeatureGroup ref={this.saveRef} >
                            <EditControl
                                position='topright'
                                onCreated={this.onDrawStop}
                                draw={{
                                    rectangle: false,
                                    polyline: false,
                                }}
                            />
                        </FeatureGroup>
                    </MapWrapper>}
            </div>

            <Button type="primary" onClick={this.onSavejson}>
                Next
            </Button>
        </div>
    }
}


export default MainSampleTab
