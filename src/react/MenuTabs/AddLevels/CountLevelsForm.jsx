
import React from 'react';
import { InputNumber, Button } from "antd"


class MainSampleTab extends React.Component {
    state = {
        levels_count: undefined
    };


    render() {

        return <div style={{ width: "100%", height: "100%" }}>
            Number of leves: <InputNumber min={1} onChange={(levels_count) => this.setState({ levels_count })} />
            <Button onClick={() => this.props.onSave(this.state.levels_count)} >Next</Button>
        </div >
    }
}


export default MainSampleTab
