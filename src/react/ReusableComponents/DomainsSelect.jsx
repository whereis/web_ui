import React, {
  forwardRef,
  useEffect,
  useState,
  memo,
  useCallback
} from "react";
import { Select } from "antd";
import { connect } from "react-redux";
import DomainsActions from "../../redux/where_is/domains/actions";
import { isEmpty } from "lodash";

const DomainsSelect = (props, ref) => {
  const {
    domains_list,
    logged_domain_name,
    get_domains,
    onChange,
    defaultValue,
    disableByLoggin,
    disabled,
    ...rest
  } = props;

  const [domain, setDomain] = useState(defaultValue);

  // effect to request domains
  useEffect(() => {
    get_domains();
  }, [get_domains]);

  // onChange middle man
  const onChangeInternal = useCallback(
    selected_domain => {
      setDomain(selected_domain);
      if (typeof onChange === "function") {
        onChange(selected_domain);
      }
    },
    [onChange]
  );

  // effect to set default selected domain
  useEffect(() => {
    if (!isEmpty(domains_list) && disableByLoggin === true && !domain) {
      const found_domain = domains_list.find(
        domain => domain.name === logged_domain_name
      );
      const domain_id = found_domain ? found_domain.id : logged_domain_name;
      onChangeInternal(domain_id);
    }
  }, [
    domains_list,
    domain,
    logged_domain_name,
    onChangeInternal,
    disableByLoggin
  ]);

  return (
    <Select
      showSearch
      ref={ref}
      style={{ width: 200 }}
      placeholder="Select Domain"
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) !== -1
      }
      {...rest}
      /* mandatory props */
      onChange={onChangeInternal}
      value={domain}
      disabled={disableByLoggin || disabled}
    >
      {domains_list.map((item, index) => (
        <Select.Option key={index} value={item.id}>
          {item.name}
        </Select.Option>
      ))}
    </Select>
  );
};

export default memo(
  connect(
    state => ({
      domains_list: state.Domains.get("domains") || [],
      logged_domain_name: state.Authentication.get("domain_name"),
      disableByLoggin: !Boolean(state.Authentication.get("write_level"))
    }),
    {
      get_domains: DomainsActions.getDomains
    },
    null,
    { forwardRef: true }
  )(forwardRef(DomainsSelect))
);
