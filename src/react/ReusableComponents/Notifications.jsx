import { notification } from "antd";

export const createNotification = (type, message, description, style) => {
  notification[type]({
    message,
    description,
    style
  });
};

