import React, { forwardRef } from "react";
import { Select } from "antd";

const select_options = ["ready", "registered", "running"];

export default forwardRef((props, ref) => {

  const select_props = {};

  if (props.hasOwnProperty("value")) {
    select_props["value"] = props["value"];
  }
  if (props.hasOwnProperty("onChange")) {
    select_props["onChange"] = props["onChange"];
  }

  return (
    <Select
      showSearch
      ref={ref}
      style={{ width: 200 }}
      placeholder="Select status"
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) !== -1
      }
      {...select_props}
    >
      {select_options.map(str => (
        <Select.Option key={str} value={str}>
          {str}
        </Select.Option>
      ))}
    </Select>
  );
});
