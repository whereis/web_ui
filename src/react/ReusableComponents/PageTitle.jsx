import React, { memo } from "react";
import { Row } from "antd";
import { withRouter } from "react-router-dom";

export default withRouter(
  memo(({ title, location }) => {
    const ptitle = title ? title : location.pathname.replace("/", "");
    return (
      <Row style={{ textAlign: "center" }}>
        <h1
          style={{
            textTransform: "capitalize"
          }}
        >
          {ptitle.replace("_", " ")}
        </h1>
      </Row>
    );
  })
);
