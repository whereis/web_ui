import React, { memo } from "react";
import SignInStyleWrapper from "./signin.style";
import { connect } from "react-redux";
import Layout from "../../react/Layout/index.jsx";

//custom
import siteIcon from "../../images/Wavecom-logo-2019.svg";
import Loginhandler from "./SignInForm/Loginhandler.jsx";

const LoginOrNot = memo(props => {
  if (props.write_level !== null) {
    return <Layout />;
  }

  return (
    <SignInStyleWrapper className="isoSignInPage">
      <div className="div-flag-version"></div>
      <div className="isoLoginContentWrapper isoLoginContent">
        <div align="middle">
          <img alt="small-logo" className="isoLogo" src={siteIcon} />
        </div>
        <div className="beauty-hr" />
        <Loginhandler />
      </div>
    </SignInStyleWrapper>
  );
});

export default connect(
  state => ({
    write_level: state.Authentication.get("write_level")
  }),
  {}
)(LoginOrNot);
