import React, { memo } from "react";
import { Icon, Input, Button, Form } from "antd";

const FormItem = Form.Item,
  IPassword = Input.Password;

function handleSubmit(e) {
  e.preventDefault();
  this.form.validateFields((err, values) => {
    if (!err) {
      // saves on local storage latest username
      localStorage.setItem("loggedUser", values["username"]);
      // passes back the form values if no errors
      this.onSubmit(values["username"], values["password"]);
    }
  });
}

const SignInForm = props => {
  const {
    form: { getFieldDecorator },
    loading
  } = props;
  // checks localstorage for username initial value
  const default_username = localStorage.getItem("loggedUser") || undefined;

  return (
    <div className="isoSignInForm">
      <Form onSubmit={handleSubmit.bind(props)} className="login-form p-top-30">
        <FormItem>
          {getFieldDecorator("username", {
            initialValue: default_username,
            rules: [{ required: true }]
          })(
            <Input
              prefix={<Icon type="user" className="icons-style" />}
              placeholder="domain"
            />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator("password", {
            rules: [
              {
                required: true
              }
            ]
          })(
            <IPassword
              prefix={<Icon type="lock" className="icons-style" />}
              placeholder="Password"
            />
          )}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            className="w-100"
            loading={loading}
          >
            {/* submit button */}
            Sign In
          </Button>
        </FormItem>
      </Form>
    </div>
  );
};

export default memo(
  // to limit component updates

  // just to have access to intl.formatMessage
  Form.create()(
    // antd form
    SignInForm
  )
);
