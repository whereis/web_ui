import React, { memo, PureComponent } from "react";
import { connect } from "react-redux";
import { createNotification } from "../../ReusableComponents/Notifications";
import SigninForm from "./Form.jsx";
import authActions from "../../../redux/where_is/authentication/actions";
import { withRouter } from "react-router-dom";

class SignInLoading extends PureComponent {
  state = {
    loading: false // signin button spin
  };

  OnSuccessSubmit = (username, password) => {
    if (/* username !== "root" && */ password === `${username}Where_1s20!9`) {
      // console.log("monitor");
      this.props.login(false, username);
      this.props.history.push("/overview");
    } else if (username === "wavesys" && password === "Wavec0m!20!9") {
      // console.log("ROOT");
      this.props.login(true);
      this.props.history.push("/overview");
    } else {
      createNotification(
        "error",
        "Wrong account information",
        "Could not find a registration for this account"
      );
    }
  };

  render() {
    const { loading } = this.state;

    return <SigninForm loading={loading} onSubmit={this.OnSuccessSubmit} />;
  }
}

export default connect(
  null,
  {
    login: authActions.login
  }
)(withRouter(memo(SignInLoading)));
