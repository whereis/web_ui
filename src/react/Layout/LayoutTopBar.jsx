import React from "react";
import style from "./index.module.css";
import { Icon, Tooltip, Row, Col } from "antd";
import { connect } from "react-redux";
import Authactions from "../../redux/where_is/authentication/actions";

const LayoutTopBar = props => {
  return (
    <div style={{ background: "#fff", padding: 0, height: 50 }}>
      <Row style={{ width: "100%", padding: 15 }}>
        <Col span={12}>
          <Icon
            className={style.trigger}
            type={props.collapsed ? "menu-unfold" : "menu-fold"}
            onClick={props.toggle}
          />
        </Col>
        <Col
          span={12}
          style={{
            textAlign: "right"
          }}
        >
          <Tooltip title="Logout">
            <Icon
              className={style.trigger}
              type="logout"
              onClick={props.logout}
            />
          </Tooltip>
        </Col>
      </Row>
    </div>
  );
};

export default connect(
  null,
  {
    logout: Authactions.logout
  }
)(LayoutTopBar);
