import React from "react";
import style from "./index.module.css";
import { Layout, Menu, Icon } from "antd";
import { Link } from "react-router-dom";
import siteIcon from "../../images/Wavecom-logo-2019.svg";
import { connect } from "react-redux";
import venuesIcon from "../customIcons/venuesIcon";
import buildingIcon from "../customIcons/buildingIcon";
import layerIcon from "../customIcons/layerIcon";
import gatewayIcon from "../customIcons/gatewayIcon";
import mapIcon from "../customIcons/mapIcon";
import assetIcon from "../customIcons/assetIcon";

const { Sider } = Layout;

const black_routes = [
  {
    route: "domains",
    text: "domains",
    icon: <Icon type="folder" />
  }
];

const white_routes = [
  /* "Sample","add_levels", */
  {
    route: "venues",
    text: "venues",
    icon: <Icon component={venuesIcon} />
  },
  {
    route: "buildings",
    text: "buildings",
    icon: <Icon component={buildingIcon} />
  },
  {
    route: "layouts",
    text: "layouts",
    icon: <Icon component={layerIcon} />
  },
  {
    route: "gateways",
    text: "gateways",
    icon: <Icon component={gatewayIcon} />
  },
  {
    route: "assets",
    text: "assets",
    icon: <Icon component={assetIcon} />
  },
  {
    route: "asset_location",
    text: "asset_location",
    icon: <Icon component={mapIcon} />
  } /* "gateway","live_test_sliding" */
];
const SiderDemo = props => {
  const routes = [
    {
      route: "overview",
      text: "overview",
      icon: <Icon type="search" />
    }
  ];
  if (props.write_level) {
    // adds root routes
    routes.push(...black_routes);
  }
  routes.push(...white_routes);
  return (
    <Sider trigger={null} collapsible collapsed={props.collapsed} width={165}>
      <div style={{ margin: "auto" }}>
        <div className={style.logo}>
          <img alt="Wavecom" src={siteIcon} />
        </div>
      </div>

      <Menu
        theme="dark"
        mode="inline"
        /* defaultSelectedKeys={defaultSelectedKeys} */
      >
        {routes.map(({ route, icon, text }) => (
          <Menu.Item key={`${route}`}>
            <Link to={`/${route}`}>
              {icon}
              <span
                style={{
                  textTransform: "capitalize"
                }}
              >
                {text.replace("_", " ")}
              </span>
            </Link>
          </Menu.Item>
        ))}
        {/* <Menu.Item key="9">
                  <Link to="/live_test">
                      <Icon type="upload" />
                      <span>Live markers</span>
                  </Link>
              </Menu.Item>
              <Menu.Item key="10">
                  <Link to="/live_test_canvas">
                      <Icon type="upload" />
                      <span>Live canvas markers</span>
                  </Link>
              </Menu.Item> */}
      </Menu>
    </Sider>
  );
};

export default connect(
  state => ({
    write_level: state.Authentication.get("write_level")
  }),
  {}
)(SiderDemo);
