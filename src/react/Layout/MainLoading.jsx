import React from "react";
import { Spin } from "antd";
import { connect } from "react-redux";
import "./mainLoadingStyle.css";

// This component shows a loading general to all pages on top of the website
// It was created separately to avoid an unecessary loading of the page it is in
class MainLoading extends React.Component {
  render() {
    return (
      <Spin
        className="SpinPositionStyle"
        size="large"
        spinning={this.props.loadingMain}
      >
        {/* This ensures correct positioning of the spin */}
        <div className="MainLoadingContentStyle" />
      </Spin>
    );
  }
}

export default connect(state => ({
  loadingMain: state.App.get("loadingMain")
}))(MainLoading);
