import React from "react";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import {
  SampleMap,
  AddLevels,
  Domains,
  EmptyComp,
  Venues,
  Buildings,
  Layouts,
  Gateways,
  Beacons,
  BeaconsStatus,
  Overview,
  TestGateways,
  LiveSlidingMapTest,
  LiveMapMarkers,
  LiveMapCanvasMarkers
} from "../MenuTabs/all_tabs";

const SiderDemo = props => {
  return (
    <Switch>
      <Route path="/" exact component={Overview} />
      {props.write_level ? (
        <Route key="/domains" path="/domains" component={Domains} />
      ) : null}
      <Route path="/overview" component={Overview} />

      <Route path="/Sample" component={SampleMap} />
      <Route path="/add_levels" component={AddLevels} />
      <Route path="/venues" component={Venues} />
      <Route path="/buildings" component={Buildings} />
      <Route path="/layouts" component={Layouts} />
      <Route path="/gateways" component={Gateways} />
      <Route path="/beacons" component={Beacons} />
      <Route path="/assets" component={Beacons} />
      <Route path="/asset_location" component={BeaconsStatus} />

      <Route path="/gateway" component={TestGateways} />
      <Route path="/live_test_sliding" component={LiveSlidingMapTest} />
      <Route path="/live_test" component={LiveMapMarkers} />
      <Route path="/live_test_canvas" component={LiveMapCanvasMarkers} />

      <Route path="/:something" component={EmptyComp} />
    </Switch>
  );
};

/* class LoadDefaultRoute extends React.PureComponent {
  componentDidMount() {
    this.props.history.push("/venues");
  }

  render() {
    return null;
  }
} */

export default connect(
  state => ({
    write_level: state.Authentication.get("write_level")
  }),
  {}
)(SiderDemo);
