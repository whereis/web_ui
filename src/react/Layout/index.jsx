import React from "react";
import { Layout } from "antd";
import Sidebar from "./Sidebar";
import Router from "./Router";
import LayoutTopBar from "./LayoutTopBar";
import PageTitle from "../ReusableComponents/PageTitle";
import MainLoading from "./MainLoading";

const { Content } = Layout;

export default class SiderDemo extends React.Component {
  state = {
    collapsed: false
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  render() {
    return (
      <Layout style={{ height: "100%" }}>
        <MainLoading />
        <Sidebar collapsed={this.state.collapsed} />
        <Layout>
          <LayoutTopBar toggle={this.toggle} collapsed={this.state.collapsed} />
          <Content
            style={{
              margin: "24px 16px",
              padding: 24,
              background: "#fff",
              minHeight: 280,
              overflow: "auto"
            }}
          >
            <PageTitle />
            <Router />
          </Content>
        </Layout>
      </Layout>
    );
  }
}
