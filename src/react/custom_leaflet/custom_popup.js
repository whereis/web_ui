//import makiValues from "./maki.json";
import L from "leaflet";
//import LGeo from "leaflet-geodesy";
import React from "react";
//import ReactDOMServer from "react-dom/server";
//import { createPortal } from "react-dom";
import ReactDOM from "react-dom";
import { Form, Input, InputNumber, Button, Switch, message, Tabs } from "antd";

const { TabPane } = Tabs;

export function bindPopup(l, writable = false, showStyle = true) {
  l.bindPopup(
    L.popup(
      {
        closeButton: true,
        maxWidth: 500,
        minWidth: 200,
        maxHeight: 400,
        autoPanPadding: [5, 45],
        className: "geojsonio-feature"
      },
      l
    )
  );

  l.on("popupopen", function(e) {
    //createPortal(<Input type="color" />, e.popup._contentNode)
    ReactDOM.render(<Componentv2 layer={l} />, e.popup._contentNode);
  });

  l.on("popupclose", e => {
    ReactDOM.unmountComponentAtNode(e.popup._contentNode);
  });
}

function handleSubmit(e) {
  /* this=props */
  e.preventDefault();
  this.form.validateFields(
    /* ['color', 'fill', 'fillColor'], */ (err, values) => {
      if (!err) {
        //console.log("Received values of form: ", values);
        this.layer.setStyle(values);
        message.success("Saved Style");
      }
    }
  );
}

const Componentv2 = Form.create()(props => {
  const { form, layer } = props;
  const { getFieldDecorator } = form;

  const {
    color,
    fill,
    fillColor,
    fillOpacity,
    opacity,
    stroke
    // weight - não sei o ue faz ainda
  } = layer.defaultOptions["style"];
  return (
    <Tabs defaultActiveKey="1">
      <TabPane tab="Tab 1" key="1">
        <Form>
          <Form.Item label="Color">
            {getFieldDecorator("color", {
              initialValue: color,
              rules: [{ required: true, message: "Required" }]
            })(<Input type="color" />)}
          </Form.Item>
          <Form.Item label="opacity">
            {getFieldDecorator("opacity", {
              initialValue: opacity,
              rules: [{ required: true, message: "Required" }]
            })(<InputNumber />)}
          </Form.Item>
          <Form.Item label="stroke">
            {getFieldDecorator("stroke", {
              initialValue: stroke,
              valuePropName: "checked",
              rules: [{ required: true, message: "Required" }]
            })(<Switch />)}
          </Form.Item>
          <Form.Item label="fill">
            {getFieldDecorator("fill", {
              initialValue: fill,
              valuePropName: "checked",
              rules: [{ required: true, message: "Required" }]
            })(<Switch />)}
          </Form.Item>
          <Form.Item label="Fill Color">
            {getFieldDecorator("fillColor", {
              initialValue: fillColor,
              rules: [{ required: true, message: "Required" }]
            })(<Input type="color" />)}
          </Form.Item>
          <Form.Item label="Fill opacity">
            {getFieldDecorator("fillOpacity", {
              initialValue: fillOpacity,
              rules: [{ required: true, message: "Required" }]
            })(<InputNumber />)}
          </Form.Item>
          <Button type="primary" onClick={handleSubmit.bind(props)}>
            Apply
          </Button>
        </Form>
      </TabPane>
      <TabPane tab="Tab 2" key="2">
        Content of Tab 2 will display properties
      </TabPane>
    </Tabs>
  );
});
