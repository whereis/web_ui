import React, { Fragment } from "react";
import {
  Map,
  TileLayer,
  Marker,
  Popup,
  FeatureGroup,
  Circle,
  MapControl,
  withLeaflet,
  Tooltip
} from "react-leaflet";
import L from "leaflet";
import IndoorLayer from "./indoor/LeafletIndoorLayer.jsx";
import IndoorLevelLayer from "./indoor/LeafletIndoorLevelLayer";
import LeafletGeoTiff from "./LeafletGeoTiff/LeafletGeoTiff.jsx";
import "leaflet-draw/dist/leaflet.draw.css";
import EditControl from "./leaflet-edit";
import { bindPopup } from "./custom_popup";
import simplestyle from "./simplestyle";
import CanvasLayer from "./ReactLeafletCanvas/CanvasLayer.js";
import CanvasMarker from "./ReactLeafletCanvas/CanvasMarker.js";
import { DriftMarker } from "leaflet-drift-marker";
import gateway_icon from "../../images/gateway_icon.svg";
import beacon_icon from "../../images/beacon_icon.svg";

const iconbeacon = new L.Icon({
  iconUrl: beacon_icon,
  iconRetinaUrl: beacon_icon,
  iconSize: new L.Point(20, 20)
});

const iconGateway = new L.Icon({
  iconUrl: gateway_icon,
  iconRetinaUrl: gateway_icon,
  iconSize: new L.Point(20, 20)
});

function GatewaysPopUpContent(props) {
  const {
    description,
    coordinates: { lat, lng }
  } = props.data;
  return (
    <Fragment>
      Location :
      <p>
        Lat:{lat}
        <br />
        Lng:{lng}
      </p>
      <p>
        Description :<span>{description}</span>
      </p>
    </Fragment>
  );
}

function BeaconsPopUpContent(props) {
  const {
    timestamp_geo,
    asset_name,
    /* locations: {
      proximity: { latitude, longitude }
    }, */
    location
  } = props.data;
  return (
    <Fragment>
      Name:<span>{asset_name}</span>
      <br />
      Location: {location}
      {/*  Lat:{latitude}
      <br />
      Lng:{longitude} */}
      <p>
        Last seen:<span>{timestamp_geo}</span>
      </p>
    </Fragment>
  );
}

export {
  /* leaflet */
  L,
  /* react-leaflet */
  Map,
  TileLayer,
  Marker,
  Popup,
  FeatureGroup,
  Circle,
  MapControl,
  withLeaflet,
  Tooltip,
  DriftMarker,
  iconGateway,
  iconbeacon,
  /* react-leaflet custom modules */
  IndoorLayer,
  IndoorLevelLayer,
  LeafletGeoTiff,
  EditControl,
  CanvasLayer,
  CanvasMarker,
  /* styles */
  simplestyle,
  bindPopup,
  /* popup Contents */
  GatewaysPopUpContent,
  BeaconsPopUpContent
};
