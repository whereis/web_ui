import React from "react";
import { withLeaflet, MapControl } from "react-leaflet";
import { Indoor } from "./leaflet-indoor/leaflet-indoor";
import data from "./leaflet-indoor/sample_data.json";
import "./leaflet-indoor/leaflet.css";

class MapInfo extends MapControl {
  componentWillUnmount() {
    if (this.indoorLayer) {
      this.props.leaflet.map.removeLayer(this.leafletElement);
    }
  }

  createLeafletElement(props) {
    this.indoorLayer = new Indoor(props.data || data, {
      hideGeoJson: props.hideGeoJson,
      getLevel: function(feature) {
        const yup = feature.properties.relations;
        if (Array.isArray(yup) && yup.length) {
          return yup[0].reltags.level;
        }
        return null;
      },
      onEachFeature: function(feature, layer) {
        if (props["no_click"] !== true) {
          layer.bindPopup(JSON.stringify(feature.properties, null, 4));
        }
      },
      style: function(feature) {
        if (feature.properties.style) {
          return feature["properties"]["style"];
        }
        return {
          fillColor: "white",
          weight: 1,
          color: "#666",
          fillOpacity: 1
        };
      }
    });
    /* if (props.hideGeoJson) {
      this.indoorLayer.hideGeoJson();
    } */
    if (props.intialLevel) {
      this.indoorLayer.setLevel(props.intialLevel);
    }

    return this.indoorLayer;
  }

  updateLeafletElement(fromProps, toProps) {
    if (fromProps.hideGeoJson !== toProps.hideGeoJson) {
      if (toProps.hideGeoJson) {
        this.leafletElement.hideGeoJson();
      }
      if (!toProps.hideGeoJson) {
        this.leafletElement.showGeoJson();
      }
    }
  }
  /* updateLeafletElement(fromProps, toProps) {
        //console.log("fromProps", fromProps)
        //console.log("toProps", toProps)

        this.indoorLayer = new Indoor(toProps.data || data, {
            getLevel: function (feature) {
                if (feature.properties.relations.length === 0)
                    return null;

                return feature.properties.relations[0].reltags.level;
            },
            onEachFeature: function (feature, layer) {
                if (toProps["no_click"] !== true) {
                    layer.bindPopup(JSON.stringify(feature.properties, null, 4));
                }
            },
            style: function (feature) {
                var fill = 'white';

                if (feature.properties.tags.buildingpart === 'corridor') {
                    fill = '#169EC6';
                } else if (feature.properties.tags.buildingpart === 'verticalpassage') {
                    fill = '#0A485B';
                }

                return {
                    fillColor: fill,
                    weight: 1,
                    color: '#666',
                    fillOpacity: 1
                };
            }
        })
        //this.indoorLayer.setLevel("0");

        toProps.leaflet.map.removeLayer(this.leafletElement);
        this.leafletElement = this.indoorLayer
        this.leafletElement.addTo(toProps.leaflet.map)
    } */

  render() {
    return React.Children.map(this.props.children, child => {
      return React.cloneElement(child, {
        indoorLayer: this.indoorLayer
      });
    });
  }
}

export default withLeaflet(MapInfo);
