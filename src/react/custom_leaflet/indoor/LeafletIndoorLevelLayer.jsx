import { withLeaflet, MapControl } from "react-leaflet";
import { IndoorLevel } from "./leaflet-indoor/leaflet-indoor"

class MapInfo extends MapControl {

    createLeafletElement(props) {
        this.levelControl = new IndoorLevel({
            indoorLayer: props.indoorLayer,
            level: props.intialLevel || "0",
            levels: props.indoorLayer.getLevels(),
            // saves current level of indoor layer
            onChange: props.onLevelChange
        });

        return this.levelControl
    }

}

export default withLeaflet(MapInfo);

