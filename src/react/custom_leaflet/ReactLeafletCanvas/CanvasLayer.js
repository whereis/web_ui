import { MapControl, withLeaflet } from "react-leaflet";
import React from 'react';
import L from "leaflet";
import { isEqual } from "lodash"
import './leaflet.canvas-markers'
import "./test.css"

export default withLeaflet(class CanvasLayer extends MapControl {
  constructor(props, context) {
    super(props);
  }

  createLeafletElement(props) {
    this.map = props.leaflet.map
    const canvasLayer = L.canvasIconLayer({})
    this.canvasLayer = canvasLayer
    this.initEventListeners(canvasLayer, props.leaflet.map);
    return canvasLayer
  }

  initEventListeners(layer, map) {
    layer.addOnClickListener((event, marker) => {
      const { options, _popup, _latlng } = marker

      options && options.onMarkerClick && options.onMarkerClick(event, marker);
      if (_popup) {
        _popup.setLatLng(_latlng).openOn(map);
      }
    });
  }

  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps.children, this.props.children)) {
      /* //console.log('momment() :', new Date()); */
      /* setTimeout(() => this.canvasLayer.redraw(), 1500);  */
      this.canvasLayer.redraw()
    }
  }

  render() {
    const { children } = this.props
    return (
      React.Children.map(children, child =>
        React.cloneElement(child, { canvasLayer: this.canvasLayer, map: this.map })
      )
    );
  }
})
