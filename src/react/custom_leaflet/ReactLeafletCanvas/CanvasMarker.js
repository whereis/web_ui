import React from 'react';
import L from "leaflet";
import './leaflet.canvas-markers'
import iconBus from "./marker_icon.svg"
import { isEqual } from "lodash"

const defaultIcon = new L.icon({
    iconUrl: iconBus,
    iconSize: [20, 18],
    iconAnchor: [10, 9],
    zIndexOffset:2
});

class CanvasMarker extends React.Component {
    constructor(props, context) {
        super(props);
    }

    componentDidMount() {
        const { canvasLayer, position, icon, popupContent, onMarkerClick } = this.props
        const marker = L.marker(position,
            { icon: (icon || defaultIcon), onMarkerClick }
        ).bindPopup(popupContent)
        this.Marker = marker
        canvasLayer.addMarker(marker, false);
    }
    componentDidUpdate(prevProps) {
        if (!isEqual(prevProps, this.props)) {
            const { canvasLayer, position, icon, popupContent, onMarkerClick } = this.props
            const marker = L.marker(position,
                { icon: (icon || defaultIcon), onMarkerClick }
            ).bindPopup(popupContent)
            canvasLayer.removeMarker(this.Marker, false)
            this.Marker = marker
            canvasLayer.addMarker(marker, false);
        }
    }

    render() {
        return (null);
    }
}


export default CanvasMarker;
