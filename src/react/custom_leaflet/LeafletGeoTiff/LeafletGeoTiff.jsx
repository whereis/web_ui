import { withLeaflet, MapControl } from "react-leaflet";
// import tiff from "../wcomtiff.tif"
import L from "leaflet";
import parse_georaster from "georaster";
import GeoRasterLayer from "georaster-layer-for-leaflet";
import { isEqual } from "lodash";
import { connect } from "react-redux";
import appActions from "../../../redux/App/actions";

function removeLoading() {
  //console.log("moveend");
  this.disableLoading();
}

class MapInfo extends MapControl {
  createLeafletElement(props) {
    props.leaflet.map.on("moveend", removeLoading.bind(props));
    return L.DomUtil.create("div", "info");
  }

  componentWillUnmount() {
    if (this.layer) {
      this.layer.remove();
    }
    this.props.leaflet.map.off("moveend", removeLoading);
  }

  componentDidUpdate({ tiff: prevTiff }) {
    ////console.log(' did update this.props.tiff :', this.props.tiff);
    const { tiff: this_tiff } = this.props;

    if (!isEqual(this_tiff, prevTiff)) {
      this.props.enableLoading();
      fetch(this.props.tiff)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => {
          parse_georaster(arrayBuffer).then(georaster => {
            //console.log("georaster:", georaster);
            if (this.layer) {
              this.layer.remove();
            }

            this.layer = new GeoRasterLayer({
              georaster: georaster,
              //opacity: 0.7,
              //pixelValuesToColorFn: values => values[0] === 42 ? '#ffffff' : '#000000',
              resolution: 720 // optional parameter for adjusting display resolution
            });

            this.layer.addTo(this.props.leaflet.map);
            this.props.leaflet.map.fitBounds(this.layer.getBounds());
          });
        });
      /* parse_georaster(this_tiff).then(georaster => {
                //console.log("georaster:", georaster);
                var layer = new GeoRasterLayer({
                    georaster: georaster,
                    //opacity: 0.7,
                    //pixelValuesToColorFn: values => values[0] === 42 ? '#ffffff' : '#000000',
                    resolution: 1280 // optional parameter for adjusting display resolution
                });
                layer.addTo(this.props.leaflet.map);
                this.props.leaflet.map.fitBounds(layer.getBounds());
            }); */
    }
  }

  componentDidMount() {
    ////console.log('did mount this.props.tiff :', this.props.tiff);

    if (this.props.tiff) {
      this.props.enableLoading();
      fetch(this.props.tiff)
        .then(response => response.arrayBuffer())
        .then(arrayBuffer => {
          parse_georaster(arrayBuffer).then(georaster => {
            //console.log("georaster:", georaster);
            this.layer = new GeoRasterLayer({
              georaster: georaster,
              //opacity: 0.7,
              //pixelValuesToColorFn: values => values[0] === 42 ? '#ffffff' : '#000000',
              resolution: 720 // optional parameter for adjusting display resolution
            });
            this.layer.addTo(this.props.leaflet.map);
            this.props.leaflet.map.fitBounds(this.layer.getBounds());
          });
        });
    }

    /* const response = await fetch(tiff)
        const response1 = await response.arrayBuffer()
        const georaster = await parse_georaster(response1)

        //console.log("georaster:", georaster);


        const a = new GeoRasterLayer({
            georaster: georaster,
            //opacity: 0.7,
            //pixelValuesToColorFn: values => values[0] === 42 ? '#ffffff' : '#000000',
            resolution: 400 // optional parameter for adjusting display resolution
        });

        a.addTo(this.props.leaflet.map); */
  }
}
export default connect(null, {
  disableLoading: appActions.disableLoading,
  enableLoading: appActions.enableLoading
})(withLeaflet(MapInfo));
