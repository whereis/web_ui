// an implementation of the simplestyle spec for polygon and linestring features
// https://github.com/mapbox/simplestyle-spec

import L from "leaflet"
import sanitize from "sanitize-caja"

var defaults = {
    color:'#555555',
    stroke: '#555555',
    'stroke-width': 2,
    'stroke-opacity': 1,
    fill: '#555555',
    'fill-opacity': 0.5
};

var mapping = [
    ['stroke', 'color'],
    ['stroke-width', 'weight'],
    ['stroke-opacity', 'opacity'],
    ['fill', 'fillColor'],
    ['fill-opacity', 'fillOpacity']
];

function remap(a) {
    var d = {};
    for (var i = 0; i < mapping.length; i++) {
        d[mapping[i][1]] = a[mapping[i][0]];
    }
    return d;
}

// a factory that provides markers for Leaflet from Mapbox's
// [simple-style specification](https://github.com/mapbox/simplestyle-spec)
// and [Markers API](http://mapbox.com/developers/api/#markers).
function marker_style(f, latlon, options) {
    return L.marker(latlon, {
        //icon: icon(f.properties, options),
        title: strip_tags(
            sanitize((f.properties && f.properties.title) || ''))
    });
}

function strip_tags(_) {
    return _.replace(/<[^<]+>/g, '');
}

export function style(feature) {
    //console.log('feature style :', feature);
    return remap(Object.assign(feature.properties || {}, defaults));
}

export const stylev2 = defaults

export default {
    style,
    stylev2,
    marker_style
}
