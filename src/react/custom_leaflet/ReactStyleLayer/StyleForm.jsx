import React from "react";
import { Form, Input, InputNumber, Switch } from "antd";

const { Item } = Form;

export default function StyleForm(props) {
  const { form, initialValue } = props;
  const { getFieldDecorator } = form;

  const {
    color,
    fill,
    fillColor,
    fillOpacity,
    opacity,
    stroke
    // weight - não sei o ue faz ainda
  } = initialValue;
  return (
    <>
      <Item label="Color">
        {getFieldDecorator("color", {
          initialValue: color,
          rules: [{ required: true, message: "Required" }]
        })(<Input type="color" />)}
      </Item>
      <Item label="opacity">
        {getFieldDecorator("opacity", {
          initialValue: opacity,
          rules: [{ required: true, message: "Required" }]
        })(<InputNumber />)}
      </Item>
      <Item label="stroke">
        {getFieldDecorator("stroke", {
          initialValue: stroke,
          valuePropName: "checked",
          rules: [{ required: true, message: "Required" }]
        })(<Switch />)}
      </Item>
      <Item label="fill">
        {getFieldDecorator("fill", {
          initialValue: fill,
          valuePropName: "checked",
          rules: [{ required: true, message: "Required" }]
        })(<Switch />)}
      </Item>
      <Item label="Fill Color">
        {getFieldDecorator("fillColor", {
          initialValue: fillColor,
          rules: [{ required: true, message: "Required" }]
        })(<Input type="color" />)}
      </Item>
      <Item label="Fill opacity">
        {getFieldDecorator("fillOpacity", {
          initialValue: fillOpacity,
          rules: [{ required: true, message: "Required" }]
        })(<InputNumber />)}
      </Item>
    </>
  );
}
