import React from "react";
import { Form, Tabs, Button, Drawer } from "antd";
import StyleForm from "./StyleForm";

const { TabPane } = Tabs;

export default Form.create()(function LayerStyleUI(props) {
  const { form, layer, visible, onClose, onSubmit } = props;
  const { validateFields } = form;
  const initialStyleValue = layer ? layer.defaultOptions["style"] : {};

  function onCloseDrawer() {
    onClose();
  }

  function onSubmitDrawerForm(e) {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        // this.layer.setStyle(values);
        onSubmit(values, layer);
      }
    });
  }

  return (
    <Drawer
      title="Style Layer"
      //width={200}
      onClose={onCloseDrawer}
      visible={visible}
      bodyStyle={{ paddingBottom: 80 }}
    >
      <Form>
        <Tabs defaultActiveKey="1">
          <TabPane tab="Tab 1" key="1">
            <StyleForm form={form} initialValue={initialStyleValue} />
          </TabPane>
          {/* <TabPane tab="Tab 2" key="2">
            Content of Tab 2 will display properties
          </TabPane> */}
        </Tabs>
        <div
          style={{
            position: "absolute",
            right: 0,
            bottom: 0,
            width: "100%",
            borderTop: "1px solid #e9e9e9",
            padding: "10px 16px",
            background: "#fff",
            textAlign: "right"
          }}
        >
          <Button onClick={onCloseDrawer} style={{ marginRight: 8 }}>
            Cancel
          </Button>
          <Button onClick={onSubmitDrawerForm} type="primary">
            Submit
          </Button>
        </div>
      </Form>
    </Drawer>
  );
});
